<li class="nav-item dropdown">
    <a id="adminDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        Administration <span class="caret"></span>
    </a>

    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="adminDropdown">
        <a class="dropdown-item" href="{{ route('user.list') }}">
            View User list
        </a>
        <a class="dropdown-item" href="{{ route('user.create') }}">
            Create users
        </a>
        <a class="dropdown-item" href="{{ route('group.manage') }}">
            Manage groups
        </a>
        <a class="dropdown-item" href="{{ route('user.fromcsv') }}">
            Import from CSV
        </a>
    </div>
</li>
