<li class="nav-item dropdown">
    <a id="teacherDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        Teacher <span class="caret"></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="teacherDropdown">
        <a class="dropdown-item" href="{{ route('test.create') }}">
            Create new test
        </a>
        <a class="dropdown-item" href="{{ route('category.manage') }}">
            Manage categories
        </a>
    </div>
</li>
