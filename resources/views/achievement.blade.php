@if (session('achievements'))
    <div aria-live="polite" aria-atomic="true" style="position: fixed; min-height: 200px;">
        <div class="mr-5 mt-5" style="position: fixed; top: 0; right: 0; max-width: 300px;">
            @foreach (session('achievements') as $a)
                <div class="toast" data-autohide="true" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <strong class="mr-auto">Achievement unlocked!</strong>
                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body">
                      <h2 class="text-center">{{ $a->name }}</h2>
                      <img src="{{ asset($a->icon) }}" class="img-fluid" alt="{{ $a->name }}">
                      <p class="lead text-center">{{ $a->description }}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif
