<div class="row mb-2">
    <div class="col-md-2"><div class="{{ $errors->has('firstname'.$n_user) ? 'has-error' : '' }}">
        <input id="firstname{{ $n_user }}" type="text" class="form-control" name="firstname{{ $n_user }}" value="{{ old('firstname'.$n_user) }}"{{ $n_user==1?' autofocus':'' }}>
        @if ($errors->has('firstname'.$n_user))
            <span class="help-block">
                <strong>{{ $errors->first('firstname'.$n_user) }}</strong>
            </span>
        @endif
    </div></div>
    <div class="col-md-2"><div class="{{ $errors->has('lastname'.$n_user) ? 'has-error' : '' }}">
        <input id="lastname{{ $n_user }}" type="text" class="form-control" name="lastname{{ $n_user }}" value="{{ old('lastname'.$n_user) }}">
        @if ($errors->has('lastname'.$n_user))
            <span class="help-block">
                <strong>{{ $errors->first('lastname'.$n_user) }}</strong>
            </span>
        @endif
    </div></div>
    <div class="col-md-3"><div class="{{ $errors->has('email'.$n_user) ? 'has-error' : '' }}">
        <input id="email{{ $n_user }}" type="email" class="form-control" name="email{{ $n_user }}" value="{{ old('email'.$n_user) }}">
        @if ($errors->has('email'.$n_user))
            <span class="help-block">
                <strong>{{ $errors->first('email'.$n_user) }}</strong>
            </span>
        @endif
    </div></div>
    <div class="col-md-2"><div class="{{ $errors->has('password'.$n_user) ? 'has-error' : '' }}">
        <input id="password{{ $n_user }}" type="password" class="form-control" name="password{{ $n_user }}" value="{{ old('password'.$n_user) }}">
        @if ($errors->has('password'.$n_user))
            <span class="help-block">
                <strong>{{ $errors->first('password'.$n_user) }}</strong>
            </span>
        @endif
    </div></div>
    <div class="col-md-1"><div class="{{ $errors->has('group'.$n_user) ? 'has-error' : '' }}">
        @include('group.select', ['name'=>"group".$n_user, 'group'=>old('group'.$n_user)])
        @if ($errors->has('group'.$n_user))
            <span class="help-block">
                <strong>{{ $errors->first('group'.$n_user) }}</strong>
            </span>
        @endif
    </div></div>
    <div class="col-md-1">
        <input id="teacher{{ $n_user }}" type="checkbox" style="margin-top: 10px;" name="teacher{{ $n_user }}" {{ old('teacher'.$n_user) }}>
    </div>
    <div class="col-md-1">
        <input id="admin{{ $n_user }}" type="checkbox" style="margin-top: 10px;" name="admin{{ $n_user }}" {{ old('admin'.$n_user) }}>
    </div>
</div>
