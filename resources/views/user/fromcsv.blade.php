@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-primary">
                <div class="card-header text-white bg-primary">Import from CSV</div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ route('user.fromcsv')}}">
                        {{ csrf_field() }}
                        <div class="row form-group">
                            <label for="csvfile" class="col-md-3 col-form-label text-right">File</label>
                            <div class="col-md-9">
                                <input id="csvfile" type="file" name="csvfile">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="skip-first-line" class="col-md-3 col-form-label text-right">Skip first line</label>
                            <div class="col-md-1">
                                <input id="skip-first-line" type="checkbox" style="margin-top: 10px;" name="skip-first-line" checked>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="delimiter" class="col-md-3 col-form-label text-right">Delimiter</label>
                            <div class="col-md-9">
                                <input id="delimiter" type="text" name="delimiter" value=",">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="enclousure" class="col-md-3 col-form-label text-right">Enclousure</label>
                            <div class="col-md-9">
                                <input id="enclousure" type="text" name="enclousure" value='"'>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="group" class="col-md-3 col-form-label text-right">Group</label>
                            <div class="col-md-9">
                                <input id="group" type="text" name="group" value=''>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="password" class="col-md-3 col-form-label text-right">Password</label>
                            <div class="col-md-9">
                                <input id="password" type="text" name="password" value=''>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-2 offset-md-5">
                                <button type="submit" class="btn btn-primary form-control">
                                    Upload
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
