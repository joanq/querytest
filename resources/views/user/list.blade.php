@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-primary">
                <div class="card-header text-white bg-primary">Users</div>
                <div class="card-body">
                    @include ('group.filter')
                    <table class="table table-striped">
                        <tr>
                            @if ($orderby=='firstname')
                                <th>Firstname <a href="{{ route('filter.usersorderby', ['orderby'=>'firstname', 'dir'=>$dir]) }}">{!! $icon !!}</a></th>
                            @else
                                <th>Firstname <a href="{{ route('filter.usersorderby', ['orderby'=>'firstname']) }}">&#10607;</a></th>
                            @endif
                            @if ($orderby=='lastname')
                                <th>Lastname <a href="{{ route('filter.usersorderby', ['orderby'=>'lastname', 'dir'=>$dir]) }}">{!! $icon !!}</a></th>
                            @else
                                <th>Lastname <a href="{{ route('filter.usersorderby', ['orderby'=>'lastname']) }}">&#10607;</a></th>
                            @endif
                            @if ($orderby=='email')
                                <th>E-Mail <a href="{{ route('filter.usersorderby', ['orderby'=>'email', 'dir'=>$dir]) }}">{!! $icon !!}</a></th>
                            @else
                                <th>E-Mail <a href="{{ route('filter.usersorderby', ['orderby'=>'email']) }}">&#10607;</a></th>
                            @endif
                            @if ($orderby=='group')
                                <th>Group <a href="{{ route('filter.usersorderby', ['orderby'=>'group', 'dir'=>$dir]) }}">{!! $icon !!}</a></th>
                            @else
                                <th>Group <a href="{{ route('filter.usersorderby', ['orderby'=>'group']) }}">&#10607;</a></th>
                            @endif
                            @if ($orderby=='points')
                                <th>Points <a href="{{ route('filter.usersorderby', ['orderby'=>'points', 'dir'=>$dir]) }}">{!! $icon !!}</a></th>
                            @else
                                <th>Points <a href="{{ route('filter.usersorderby', ['orderby'=>'points']) }}">&#10607;</a></th>
                            @endif
                            <th>Roles</th>
                            <th>Actions</th>
                        </tr>
                        @foreach ($users as $user)
                            <tr>
                                <td><a href="{{ route('user.profile', ['user'=>$user->id]) }}">{{ $user->firstname }}</a></td>
                                <td><a href="{{ route('user.profile', ['user'=>$user->id]) }}">{{ $user->lastname }}</a></td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if (!empty($user->group))
                                       {{ $user->group->name }}
                                    @endif
                                </td>
                                <td>{{ $user->points }}</td>
                                <td>{{ $user->hasRole('Teacher')?'Teacher ':'' }}
                                    {{ $user->isAdmin()?'Admin':'' }}</td>
                                <td><a class="btn btn-primary btn-sm" href="{{ route('user.edit', ['user' => $user->id]) }}">
                                        <svg class="feather" aria-hidden="true" aria-label="Edit">
                                            <use xlink:href="{{ asset('images/feather-sprite.svg#edit') }}"/>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="{{ route('user.delete', ['user' => $user->id]) }}">
                                        <svg class="feather" aria-hidden="true" aria-label="Delete">
                                            <use xlink:href="{{ asset('images/feather-sprite.svg#delete') }}"/>
                                        </svg>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {!! $users->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
