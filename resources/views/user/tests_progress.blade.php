<div class="row">
    <div class="col-md-12">
        <div class="card border-primary mb-3">
            <div class="card-header text-white bg-primary">Tests Progress</div>
            <div class="card-body">
                @php
                    $classes=['badge-secondary', 'badge-danger', 'badge-success', 'badge-info'];
                    $texts=['-', 'F', 'S', 'D'];
                @endphp
                @foreach($tests as $test)
                    <div class="row">
                        <div class="col-md-1">
                            @if ($test->isClosed($user->group))
                                <span class="badge badge-danger">Closed</span>
                            @else
                                <span class="badge badge-success">Open</span>
                            @endif
                        </div>
                        <div class="col-md-4">
                            @if (Auth::user()->hasRole('Teacher'))
                                <a class="btn btn-outline-secondary btn-sm" href="{{ route('testuser.attempts', ['test' => $test->id,'user'=>$user->id]) }}">
                                    <svg class="feather" aria-hidden="true" aria-label="Review">
                                        <use xlink:href="{{ asset('images/feather-sprite.svg#eye') }}"/>
                                    </svg>
                                </a>
                            @endif
                            <a href="{{ route('test.show', ['test' => $test->id]) }}">{{ $test->title }}</a>:
                        </div>
                        <div class="col-md-7">
                            @foreach($test->results as $result)
                                <span class="fixed-width-30 badge {{ $classes[$result] }}">{{ $texts[$result] }}</span>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
