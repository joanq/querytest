@extends('layouts.app')

@section('content')
<div class="container">
    @include('user.main_profile')
    @include('user.tests_progress')
    @include('user.achievements')
</div>
@endsection
