@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-danger">
                <div class="card-header text-white bg-danger">Confirm deletion</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">Name: <strong>{{ $user->name }}</strong></div>
                        <div class="col-md-6">E-Mail: <strong>{{ $user->email }}</strong></div>
                        <div class="col-md-12">
                            <form class="form-horizontal" method="POST" action="{{ route('user.delete', ['user' => $user->id]) }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="col-md-2 offset-md-5 mt-4">
                                        <button type="submit" class="btn btn-danger btn-block">
                                            Confirm
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
