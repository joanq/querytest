<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card border-primary">
            <div class="card-header text-white bg-primary">Achievements</div>
            <div class="card-body">
                <div class="row">
                    @foreach($user->unlockedAchievements() as $achievementProgress)
                        @php $a=$achievementProgress->details->getclass(); @endphp
                        <div class="col-md-4">
                            <h2 class="text-center">{{ $a->name }}</h2>
                            <img src="{{ asset($a->icon) }}" class="img-fluid" alt="{{ $a->name }}">
                            <p class="lead text-center">{{ $a->description }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
