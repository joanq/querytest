<div class="row">
    <div class="col-md-12">
        <div class="card border-primary mb-3">
            <div class="card-header text-white bg-primary">{{ $user->name }}</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <strong>Email: </strong>{{ $user->email }}
                    </div>
                    <div class="col-md-5">
                        <strong>Last Login: </strong>
                        @empty($user->last_login_at)
                            Never
                        @else
                            {{ $user->last_login_at }} ({{ $user->last_login_ip}})
                        @endempty
                    </div>
                    <div class="col-md-3">
                        @if (!empty($user->group))
                            <strong>Group: </strong>{{ $user->group->name }}
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Solved Questions: </strong>{{ $user->testsSolved()->sum('nSolved') }}
                    </div>
                    <div class="col-md-5">
                        <strong>Total Points: </strong>{{ $user->points }}
                    </div>
                    @if (Auth::user()->isAdmin())
                        <div class="col-md-3">
                           <a class="btn btn-primary" href="{{ route('user.edit', ['user' => $user->id]) }}">
                               <svg class="feather" aria-hidden="true" aria-label="Edit">
                                   <use xlink:href="{{ asset('images/feather-sprite.svg#edit') }}"/>
                               </svg>
                           </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
