@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-primary">
                <div class="card-header text-white bg-primary">Edit user</div>
                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('user.edit', ['user' => $user->id]) }}">
                        {{ csrf_field() }}
                        <div class="row form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <label for="firstname" class="col-md-2 col-form-label text-right">FirstName</label>
                            <div class="col-md-8">
                                <input id="firstname" type="text" class="form-control" name="firstname" value="{{ $user->firstname ?? old('firstname') }}" required>
                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-2 col-form-label text-right">LastName</label>
                            <div class="col-md-8">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ $user->lastname ?? old('lastname') }}">
                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-2 col-form-label text-right">E-Mail</label>
                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email ?? old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-2 col-form-label text-right">Password</label>
                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group{{ $errors->has('group') ? ' has-error' : '' }}">
                            <label for="group" class="col-md-2 col-form-label text-right">Group</label>
                            <div class="col-md-8">
                                @include('group.select', ['name'=>"group",
                                    'group'=>(null!==old('group')?old('group'):
                                        ((isset($user->group))?$user->group->name:''))])
                                @if ($errors->has('group'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('group') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group{{ $errors->has('points') ? ' has-error' : '' }}">
                            <label for="points" class="col-md-2 col-form-label text-right">Points</label>
                            <div class="col-md-8">
                                <input id="points" type="text" class="form-control" name="points" value="{{ $user->points ?? old('points') }}">
                                @if ($errors->has('points'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('points') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="teacher" class="col-md-2 col-form-label text-right">Teacher</label>
                            <div class="col-md-1">
                              <input id="teacher" type="checkbox" style="margin-top: 10px;" name="teacher" {{ $user->hasRole('Teacher')?' checked':'' }}>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="admin" class="col-md-2 col-form-label text-right">Admin</label>
                            <div class="col-md-1">
                              <input id="admin" type="checkbox" style="margin-top: 10px;" name="admin" {{ $user->isAdmin()?' checked':'' }}>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-2 offset-md-5">
                                <button type="submit" class="btn btn-primary form-control">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
