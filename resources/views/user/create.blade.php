@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border-primary">
                <div class="card-header text-white bg-primary">Create new users</div>
                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('user.create') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-2">FirstName</div>
                            <div class="col-md-2">LastName</div>
                            <div class="col-md-3">E-Mail</div>
                            <div class="col-md-2">Password</div>
                            <div class="col-md-1">Group</div>
                            <div class="col-md-1">Teacher</div>
                            <div class="col-md-1">Admin</div>
                        </div>
                        @for ($n_user=1; $n_user<=12; $n_user++)
                            @include('user.user_form')
                        @endfor
                        <div class="form-group">
                            <div class="col-md-2 offset-md-5">
                                <button type="submit" class="btn btn-primary form-control">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
