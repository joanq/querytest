@if (Auth::user()->hasRole('Teacher'))
  <div class="row"><div class="col-md-12">
    <form class="form-inline" id="filter-by-group-form" action="{{ route('filter.group') }}" method="POST">
        {{ csrf_field() }}
        <label for="group">Group: </label>
        <select id="group" name="group" class="form-control">
            <option @if ($group=='All') selected @endif value="All">All</option>
            @foreach ($groups as $gr)
                <option @if ($group==$gr->name) selected @endif value="{{ $gr->name }}">{{ $gr->name }}</option>
            @endforeach
        </select>
        <button type="submit" class="btn btn-outline-secondary">Filter</button>
    </form>
  </div></div><br>
@endif
