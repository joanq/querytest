@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-primary mb-3">
                <div class="card-header text-white bg-primary">Groups</div>
                <div class="card-body">
                    @if ($errors->has('edname'))
                        <div class="alert alert-danger">
                            <strong>{{ $errors->first('edname') }}</strong>
                        </div>
                    @endif
                    @if ($errors->has('eddescription'))
                        <div class="alert alert-danger">
                            <strong>{{ $errors->first('eddescription') }}</strong>
                        </div>
                    @endif
                    <div class="row">
                        <div class="offset-md-2 col-md-2"><strong>Name</strong></div>
                        <div class="col-md-4"><strong>Description</strong></div>
                    </div>
                    @forelse ($groups as $group)
                        <form class="row form-horizontal" method="POST" action="{{ route('group.edit', ['group' => $group->id]) }}">
                            {{ csrf_field() }}
                            <div class="offset-md-2 col-md-2">
                                <input type="text" class="form-control" name="edname" value="{{ $group->name }}" required>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="eddescription" value="{{ $group->description }}">
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <svg class="feather" aria-hidden="true" aria-label="Edit">
                                        <use xlink:href="{{ asset('images/feather-sprite.svg#edit') }}"/>
                                    </svg>
                                </button>
                                <a class="btn btn-danger btn-sm" href="{{ route('group.delete', ['group' => $group->id]) }}">
                                    <svg class="feather" aria-hidden="true" aria-label="Delete">
                                        <use xlink:href="{{ asset('images/feather-sprite.svg#delete') }}"/>
                                    </svg>
                                </a>
                            </div>
                        </form>
                    @empty
                        <div class="row"><div class="col-md-12">There aren't any groups yet.</div></div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card border-primary">
                <div class="card-header text-white bg-primary">Create new group</div>
                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('group.create') }}">
                        {{ csrf_field() }}
                        <div class="row form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-2 col-form-label text-right">Name</label>
                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $name ?? old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-2 col-form-label text-right">Description</label>
                            <div class="col-md-8">
                                <input id="description" class="form-control" name="description" value="{{ $description ?? old('description') }}">
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-2 offset-md-5">
                                <button type="submit" class="btn btn-primary form-control">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
