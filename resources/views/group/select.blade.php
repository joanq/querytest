<select id="{{ $name }}" name="{{ $name }}" class="form-control">
    <option @if ($group=='') selected @endif value=""></option>
    @foreach (App\Group::all() as $gr)
        <option @if ($group==$gr->name) selected @endif value="{{ $gr->name }}">{{ $gr->name }}</option>
    @endforeach
</select>
