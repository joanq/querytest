@if ($test->areResultsShown(Auth::user()->group) || Auth::user()->hasRole('Teacher'))
  <div class="row"><div class="col-md-offset-2 col-md-8">
    Solution: <br>
    {!! Markdown::convertToHtml("```sql\n".$question->solution."\n```") !!}
  </div></div>
@endif
<div class="row"><div class="col-md-offset-2 col-md-6">
  @if ($test->areResultsShown(Auth::user()->group))
    @if ($question->attempts_count==0)
      You haven't answered this question.
    @elseif ($question->solved>0)
      <span class="text-success">You solved this question!</span>
    @else
      <span class="text-warning">You didn't solved this question.</span>
    @endif
  @else
    @if ($question->attempts_count==0)
      You didn't answer this question.
    @else
      <span class="text-info">You answered this question.</span>
    @endif
  @endif
</div>
<div class="col-md-2 text-right">
  @if (Auth::user()->hasRole('Teacher'))
    <a class="btn btn-secondary" href="{{ route('question.reevaluate', ['question' => $question->id]) }}" data-toggle="tooltip" title="Reevaluate">
        <svg class="feather text-white" aria-hidden="true" aria-label="View attempts">
            <use xlink:href="{{ asset('images/feather-sprite.svg#refresh-cw') }}"/>
        </svg>
    </a>
    <a class="btn btn-default" href="{{ route('question.attempts', ['question' => $question->id]) }}" data-toggle="tooltip" title="View attempts">
        <svg class="feather text-white" aria-hidden="true" aria-label="View attempts">
            <use xlink:href="{{ asset('images/feather-sprite.svg#zoom-in') }}"/>
        </svg>
    </a>
  @else
    <a class="btn btn-default" href="{{ route('user.attempts', ['user' => Auth::user()->id, 'question' => $question->id]) }}" data-toggle="tooltip" title="View attempts">
        <svg class="feather text-white" aria-hidden="true" aria-label="View attempts">
            <use xlink:href="{{ asset('images/feather-sprite.svg#zoom-in') }}"/>
        </svg>
    </a>
  @endif
</div></div>
