<form class="form-horizontal" method="POST" action="{{ route('question.answer', ['question' => $question->id]) }}">
    {{ csrf_field() }}
    <div class="row form-group{{ session('questionid')===$question->id ? ' has-'.session('message')->type : '' }}">
        <label for="answer{{ $question->id }}" class="col-md-2 col-form-label text-right">Your answer</label>
        <div class="col-md-8">
            <textarea id="answer{{ $question->id }}" class="form-control" name="answer{{ $question->id }}"
                cols="60" rows="5">{{ old('answer'.$question->id) }}</textarea>
            @if (session('questionid')===$question->id)
                <span class="help-block">
                    <strong>{{ session('message')->message }}  </strong>
                    @if (session('questionPoints')!=0)
                        <strong>You win {{ session('questionPoints') }} points!</strong>
                    @endif
                    @if (session('testPoints')!=0)
                        <strong>You win {{ session('testPoints') }} points for completing the whole test!!</strong>
                    @endif
                </span>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-1 offset-md-2">
            <button type="submit" class="btn btn-primary">
                Send
            </button>
        </div>
        <div class="col-md-5">
            @if ($test->areResultsShown(Auth::user()->group)==true)
              @if ($question->attempts_count==0)
                Not yet tried.
              @elseif ($question->solved>0)
                @if ($question->attempts_count==1)
                  <span class="text-success">Solved at the first try!</span>
                @else
                  <span class="text-success">Already solved (using {{ $question->attempts_count }} attempt{{ $question->attempts_count==1?'':'s' }}).</span>
                @endif
              @else
                @if ($question->hint!='')
                  <button class="btn btn-secondary hint-btn" type="button" id="hint-btn-{{ $question->id }}">Show hint</button>
                @endif
                <span class="text-warning">Tried {{ $question->attempts_count }} time{{ $question->attempts_count==1?'':'s' }}.</span>
              @endif
            @else
              @if ($question->attempts_count==0)
                Not yet answered.
              @else
                <span class="text-info">Already answered.</span>
              @endif
            @endif
        </div>
        <div class="col-md-2 text-right">
        @if (Auth::user()->hasRole('Teacher'))
            <a class="btn btn-secondary" href="{{ route('question.reevaluate', ['question' => $question->id]) }}" data-toggle="tooltip" title="Reevaluate">
                <svg class="feather text-white" aria-hidden="true" aria-label="Reevaluate">
                    <use xlink:href="{{ asset('images/feather-sprite.svg#refresh-cw') }}"/>
                </svg>
            </a>
            <a class="btn btn-secondary" href="{{ route('question.attempts', ['question' => $question->id]) }}" data-toggle="tooltip" title="View attempts">
                <svg class="feather text-white" aria-hidden="true" aria-label="View attempts">
                    <use xlink:href="{{ asset('images/feather-sprite.svg#zoom-in') }}"/>
                </svg>
            </a>
        @else
            <a class="btn btn-secondary" href="{{ route('user.attempts', ['user' => Auth::user()->id, 'question' => $question->id]) }}"  data-toggle="tooltip" title="View attempts">
                <svg class="feather text-white" aria-hidden="true" aria-label="View attempts">
                    <use xlink:href="{{ asset('images/feather-sprite.svg#zoom-in') }}"/>
                </svg>
            </a>
        @endif
        </div>
    </div>
    <div class="row" id="hint-text-{{ $question->id }}" style="display:none">
      <div class="col-md-8 offset-md-2"><span class="font-weight-bold"></span></div>
    </div>
</form>
