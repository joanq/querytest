@extends('layouts.app')

@section('content')
<div class="container">
    @include ('test.header_panel')
    <div class="row"><div class="col-md-12"><div class="card border-danger">
        <div class="card-header text-white bg-danger">Reevaluate question</div>
        <div class="card-body">
            <div class="row"><div class="col-md-12">
                <p>This will reevaluate ALL attempts to this question. No points will be rewarded or substracted from users.</p>
                <p>There are {{ $n_attempts }} attempts to this question, from which {{ $modified }} will be modified.</p>
                <form class="form-horizontal" method="POST" action="{{ route('question.reevaluate', ['question' => $question->id]) }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="col-md-2 offset-md-5">
                            <button type="submit" class="btn btn-danger btn-block">
                                Confirm
                            </button>
                        </div>
                    </div>
                </form>
            </div></div>
        </div>
    </div></div></div>
</div>
@endsection
