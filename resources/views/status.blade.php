@if (session('status'))
    <div class="container"><div class="row"><div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            @if (gettype(session('status'))=='string')
                {{ session('status') }}
            @else
                <ul>
                @foreach (session('status')->all() as $status)
                    <li>{{ $status }}</li>
                @endforeach
                </ul>
            @endif
        </div>
    </div></div></div>
@endif
