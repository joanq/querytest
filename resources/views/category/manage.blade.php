@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-primary mb-3">
                <div class="card-header text-white bg-primary">Categories</div>
                <div class="card-body">
                    @if ($errors->has('edname'))
                        <div class="alert alert-danger">
                            <strong>{{ $errors->first('edname') }}</strong>
                        </div>
                    @endif
                    <div class="row">
                        <div class="offset-md-2 col-md-2"><strong>Category</strong></div>
                    </div>
                    @forelse ($categories as $category)
                        <form id="drop-{{ $category->id }}" ondragover="event.preventDefault()" ondrop="drop(event, 'category')"
                          class="row form-horizontal" method="POST" action="{{ route('category.edit', ['category' => $category->id]) }}">
                            {{ csrf_field() }}
                            <div class="offset-md-1 col-md-1 text-right">
                                <a class="btn" id="drag-{{ $category->id }}" draggable="true" ondragstart="drag(event)" style="cursor: move">&#8691;</a>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="edname" value="{{ $category->name }}" required>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <svg class="feather" aria-hidden="true" aria-label="Edit">
                                        <use xlink:href="{{ asset('images/feather-sprite.svg#edit') }}"/>
                                    </svg>
                                </button>
                                @if ($category->id!=1)
                                    <a class="btn btn-danger btn-sm" href="{{ route('category.delete', ['category' => $category->id]) }}">
                                        <svg class="feather" aria-hidden="true" aria-label="Delete">
                                            <use xlink:href="{{ asset('images/feather-sprite.svg#delete') }}"/>
                                        </svg>
                                    </a>
                                @endif
                            </div>
                        </form>
                    @empty
                        <div class="row"><div class="col-md-12">There aren't any categories yet.</div></div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card border-primary">
                <div class="card-header text-white bg-primary">Create new category</div>
                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('category.create') }}">
                        {{ csrf_field() }}
                        <div class="row form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-2 col-form-label text-right">Name</label>
                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $name ?? old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2 offset-md-5">
                                <button type="submit" class="btn btn-primary form-control">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script src="{{ asset('js/move_item.js') }}"></script>
@endsection
