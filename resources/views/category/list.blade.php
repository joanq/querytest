<select id="category" name="category" class="form-control">
    @foreach ($categories as $cat)
        <option @if ($category==$cat->name) selected @endif value="{{ $cat->id }}">{{ $cat->name }}</option>
    @endforeach
</select>
