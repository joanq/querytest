<div class="row">
    <div class="col-md-12">
        <div class="card border-primary mb-3">
            <div class="card-header text-white bg-primary">{{ isset($question)?'Question':'Test' }}</div>

            <div class="card-body">
                Test: <a href="{{ route('test.show', ['test' => $test->id]) }}">{{ $test->title }}</a><br>
                Database: {{ $test->database }}<br>
                @isset($question)
                    Question: <br>
                    {!! Markdown::convertToHtml($question->question) !!}<br>
                @endif
                @if (Auth::user()->hasRole('Teacher'))
                    @isset($question)
                        Answer: <br>
                        {{ $question->solution }}<br>
                        <a class="btn btn-outline-secondary" href="{{ route('test.attempts', ['test' => $test->id]) }}">View all attempts</a>
                    @endif
                    <a class="btn btn-outline-secondary" href="{{ route('test.attempts', ['test' => $test->id]) }}">View attempts</a>
                    <a class="btn btn-outline-secondary" href="{{ route('test.summary', ['test' => $test->id]) }}">View summary</a>
                @endif
            </div>
        </div>
    </div>
</div>
