<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card border-default">
            <div class="card-header">All attempts for this {{ isset($question)?'question':'test' }}</div>
            <div class="card-body">
              @include ('group.filter')
              @if (count($attempts)>0)
                  <table class="table">
                      <tr>
                          @if ($orderby=='date')
                              <th>Time <a href="{{ route('filter.orderby', ['orderby'=>'date', 'dir'=>$dir]) }}">{!! $icon !!}</a></th>
                          @else
                              <th>Time <a href="{{ route('filter.orderby', ['orderby'=>'date']) }}">&#10607;</a></th>
                          @endif
                          @if ($orderby=='user')
                              <th>User <a href="{{ route('filter.orderby', ['orderby'=>'user', 'dir'=>$dir]) }}">{!! $icon !!}</a></th>
                          @else
                              <th>User <a href="{{ route('filter.orderby', ['orderby'=>'user']) }}">&#10607;</a></th>
                          @endif
                          @if ($test->areResultsShown(Auth::user()->group) || Auth::user()->hasRole('Teacher'))
                              @if ($orderby=='message')
                                  <th>Result <a href="{{ route('filter.orderby', ['orderby'=>'message', 'dir'=>$dir]) }}">{!! $icon !!}</a></th>
                              @else
                                  <th>Result <a href="{{ route('filter.orderby', ['orderby'=>'message']) }}">&#10607;</a></th>
                              @endif
                          @endif
                          @empty ($question)
                              @if ($orderby=='question')
                                  <th>Question <a href="{{ route('filter.orderby', ['orderby'=>'question', 'dir'=>$dir]) }}">{!! $icon !!}</a></th>
                              @else
                                  <th>Question <a href="{{ route('filter.orderby', ['orderby'=>'question']) }}">&#10607;</a></th>
                              @endif
                          @endempty
                          <th>Answer</th>
                          @if (Auth::user()->hasRole('Teacher'))
                            <th>Delete</th>
                          @endif
                      </tr>
                      @foreach ($attempts as $attempt)
                          @if ($test->areResultsShown(Auth::user()->group) || Auth::user()->hasRole('Teacher'))
                            <tr class="{{ $attempt->message->type=='error'?'alert-danger':'alert-'.$attempt->message->type }}">
                          @else
                            <tr>
                          @endif
                              <td>{{ $attempt->date }}</td>
                              <td><a href="{{ route('user.profile', ['user'=>$attempt->user->id]) }}">{{ $attempt->user->name }}</a></td>
                              @if ($test->areResultsShown(Auth::user()->group) || Auth::user()->hasRole('Teacher'))
                                  <td>{{ $attempt->message->code }}</td>
                              @endif
                              @empty ($question)
                                  <td>{{ $attempt->question->question }}</td>
                              @endempty
                              <td>{{ $attempt->answer }}</td>
                              @if (Auth::user()->hasRole('Teacher'))
                                <td><a class="btn btn-danger" href="{{ route('attempt.delete', ['attempt' => $attempt->id]) }}">
                                    <svg class="feather" aria-hidden="true" aria-label="Delete">
                                        <use xlink:href="{{ asset('images/feather-sprite.svg#delete') }}"/>
                                    </svg>
                                </a></td>
                              @endif
                          </tr>
                      @endforeach
                  </table>
                  {!! $attempts->links() !!}
              @else
                  <div class="alert alert-warning">
                      There aren't any attempts.
                  </div>
              @endif
            </div>
        </div>
    </div>
</div>
