@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-primary mb-3">
                <div class="card-header text-white bg-primary">Test</div>
                <div class="card-body">
                    Database: <strong>{{ $test->database }}</strong><br>
                    Questions: {{ count($test->questions) }}<br>
                    Description:<br>
                    {!! Markdown::convertToHtml($test->description ?? '') !!}<br>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card border-primary">
                <div class="card-header text-white bg-primary">Test visibility</div>
                <div class="card-body">
                    <div class="row text-center">
                        <div class="col-md-2"><strong>Group</strong></div>
                        <div class="col-md-2"><strong>Published</strong></div>
                        <div class="col-md-3"><strong>Opened from</strong></div>
                        <div class="col-md-3"><strong>Opened until</strong></div>
                        <div class="col-md-2"><strong>Hide Results</strong></div>
                    </div>
                    <form method="post" action="{{ route('test.changevisibility', ['test' => $test->id] )}}">
                        @csrf
                        @foreach ($groups as $group)
                            <div class="row text-center mt-1">
                                <div class="col-md-2">
                                    {{ $group->name }}
                                    <input type="hidden" name="group_id[]" value="{{ $group->id }}">
                                </div>
                                <div class="col-md-2">
                                    <input type="checkbox" name="published[]" value="{{ $group->id }}" {{ $test->isPublished($group)?' checked':'' }}>
                                </div>
                                <div class="col-md-3">
                                    <input type="date" name="open_from_date[]" value="{{ $test->openFrom($group)?$test->openFrom($group)->toDateString():'' }}">
                                    <input type="time" name="open_from_time[]" value="{{ $test->openFrom($group)?$test->openFrom($group)->format('H:i'):'' }}">
                                </div>
                                <div class="col-md-3">
                                    <input type="date" name="open_until_date[]" value="{{ $test->openUntil($group)?$test->openUntil($group)->toDateString():'' }}">
                                    <input type="time" name="open_until_time[]" value="{{ $test->openUntil($group)?$test->openUntil($group)->format('H:i'):'' }}">
                                </div>
                                <div class="col-md-2">
                                    <input type="checkbox" name="hide_results[]" value="{{ $group->id }}" {{ $test->areResultsHidden($group)?' checked':'' }}>
                                </div>
                            </div>
                        @endforeach
                        <div class="row text-right mt-4">
                            <div class="offset-md-10 col-md-2">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
