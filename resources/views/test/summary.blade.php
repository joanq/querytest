@extends('layouts.app')

@section('content')
<div class="container">
    @include ('test.header_panel')
    <div class="row">
        <div class="col-md-12">
            <div class="card border-default">
                <div class="card-header">Summary for this test</div>
                <div class="card-body">
                    @include ('group.filter')
                    @if (count($users)>0)
                        <table class="table table-striped">
                            <tr>
                                <th>User</th>
                                @for ($q=1; $q<=$questions->count(); $q++)
                                    <th>{{ $q }}</th>
                                @endfor
                            </tr>
                            @php $nRow=0; $classes=['', 'alert-danger', 'alert-success']; $texts=['Not tried', 'Failed', 'Solved']; @endphp
                            @foreach ($users as $user)
                                <tr><td>
                                    <a href="{{ route('user.profile', ['user'=>$user->id]) }}">{{ $user->firstname }} {{ $user->lastname }}</a>
                                    <a class="btn btn-outline-secondary" href="{{ route('testuser.attempts', ['test' => $test->id,'user'=>$user->id]) }}">
                                        <svg class="feather" aria-hidden="true" aria-label="Review">
                                            <use xlink:href="{{ asset('images/feather-sprite.svg#eye') }}"/>
                                        </svg>
                                    </a>
                                    </td>
                                @php $row=$results->where('userid',$user->id); @endphp
                                @foreach ($questions as $question)
                                    @php $cell=$row->where('questionid', $question->id)->first(); @endphp
                                    @if (!is_null($cell))
                                        <td class="{{ $classes[$cell->message] }}">
                                            <a href="{{ route('user.attempts', ['question' => $question->id, 'user' => $user->id]) }}">
                                                {{ $texts[$cell->message] }} ({{ $cell->nTries }})
                                            </a>
                                        </td>
                                    @else
                                        <td>Not tried</td>
                                    @endif
                                @endforeach
                                </tr>
                            @endforeach
                        </table>
                        {{ $users->links() }}
                    @else
                        <div class="alert alert-warning">
                            There aren't any attempts.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
