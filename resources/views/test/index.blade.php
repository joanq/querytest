@if ($test->isPublished(Auth::user()->group) || Auth::user()->hasRole('Teacher'))
    @if (Auth::user()->hasRole('Teacher'))
        <div class="row" id="drop-{{ $test->id }}" ondragover="event.preventDefault()" ondrop="drop(event, 'test')">
            <div class="col-md-5">
              <a class="btn text-info" id="drag-{{ $test->id }}" draggable="true" ondragstart="drag(event)" style="cursor: move">&#8691;</a>
              {{ $test->title }}
            </div>
            <div class="col-md-4">
                @foreach ($groups as $group)
                    @if ($test->isPublished($group))
                        @if ($test->isOpen($group))
                            @if ($test->areResultsShown($group))
                                <span class="badge badge-success">{{ $group->name }}</span>
                            @else
                                <span class="badge badge-primary">{{ $group->name }}</span>
                            @endif
                        @else
                            <span class="badge badge-danger">{{ $group->name }}</span>
                        @endif
                    @endif
                @endforeach
            </div>
            <div class="col-md-3 text-right">
                <a class="btn btn-sm btn-primary" href="{{ route('test.show', ['test' => $test->id]) }}">
                    <svg class="feather text-white" aria-hidden="true" aria-label="View">
                        <use xlink:href="{{ asset('images/feather-sprite.svg#play') }}"/>
                    </svg>
                </a>
                <a class="btn btn-default btn-sm btn-outline-secondary" href="{{ route('test.edit', ['test' => $test->id]) }}">
                    <svg class="feather" aria-hidden="true" aria-label="Edit">
                        <use xlink:href="{{ asset('images/feather-sprite.svg#edit') }}"/>
                    </svg>
                </a>
                <a class="btn btn-default btn-sm btn-outline-secondary" href="{{ route('test.visibility', ['test' => $test->id]) }}">
                    <svg class="feather" aria-hidden="true" aria-label="Visibility">
                        <use xlink:href="{{ asset('images/feather-sprite.svg#eye-off') }}"/>
                    </svg>
                </a>
                <a class="btn btn-default btn-sm btn-outline-secondary" href="{{ route('test.clone', ['test' => $test->id]) }}">
                    <svg class="feather" aria-hidden="true" aria-label="Clone">
                        <use xlink:href="{{ asset('images/feather-sprite.svg#copy') }}"/>
                    </svg>
                </a>
                <a class="btn btn-danger btn-sm" href="{{ route('test.delete', ['test' => $test->id]) }}">
                    <svg class="feather text-white" aria-hidden="true" aria-label="Delete">
                        <use xlink:href="{{ asset('images/feather-sprite.svg#delete') }}"/>
                    </svg>
                </a>
    @else
        <div class="row mb-1">
            <div class="col-md-5">{{ $test->title }}</div>
            <div class="col-md-4">
                @php
                    $group = Auth::user()->group;
                    $openFrom = $test->openFrom($group);
                    $openUntil = $test->openUntil($group);
                    $now = Carbon\Carbon::now();
                    if ($test->isClosed($group)) {
                        if (isset($openUntil) && $openUntil->lessThanOrEqualTo($now)) {
                            echo "Closed from $openUntil.";
                        } else {
                            echo "Closed.";
                        }
                    } else {
                        if (isset($openUntil)) {
                            echo "Open until $openUntil.";
                        } else {
                            echo "Open.";
                        }
                    }
                @endphp
            </div>
            <div class="col-md-3 text-right">
                @if ($test->isClosed(Auth::user()->group))
                    <a class="btn btn-secondary btn-sm" href="{{ route('test.show', ['test' => $test->id]) }}">
                        <svg class="feather" aria-hidden="true" aria-label="Review">
                            <use xlink:href="{{ asset('images/feather-sprite.svg#eye') }}"/>
                        </svg>
                    </a>
                @else
                    <a class="btn btn-success btn-sm" href="{{ route('test.show', ['test' => $test->id]) }}">
                        <svg class="feather" aria-hidden="true" aria-label="Go">
                            <use xlink:href="{{ asset('images/feather-sprite.svg#play') }}"/>
                        </svg>
                    </a>
                @endif
    @endif
        </div>
    </div>
@endif
