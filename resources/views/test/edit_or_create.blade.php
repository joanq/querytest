{{ csrf_field() }}

<div class="row form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 col-form-label text-right">Title</label>

    <div class="col-md-8">
        <input id="title" type="text" class="form-control" name="title" value="{{ $title ?? old('title') }}" required autofocus>

        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label for="description" class="col-md-2 col-form-label text-right">Description</label>

    <div class="col-md-8">
        <textarea id="description" class="form-control" name="description" maxlength="65535" cols="60" rows="5">{{ $description ?? old('description') }}</textarea>

        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row form-group{{ $errors->has('database') ? ' has-error' : '' }}">
    <label for="database" class="col-md-2 col-form-label text-right">Database</label>

    <div class="col-md-8">
        <input id="database" type="text" class="form-control" name="database" value="{{ $database ?? old('database') }}" required>

        @if ($errors->has('database'))
            <span class="help-block">
                <strong>{{ $errors->first('database') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row form-group">
    <label for="category" class="col-md-2 col-form-label text-right">Category</label>
    <div class="col-md-8">
        @include ('category.list', ['categories'=>$categories, 'category'=>$category])
    </div>
</div>

<div id="questions">
@for ($n_question=1; $n_question<=50; $n_question++)
    <div id="question-group{{ $n_question }}" class="question-group" style="{{ $n_question>(isset($n_questions)?$n_questions:5)?'display:none':'' }}">
        <div class="row form-group{{ $errors->has('question'.$n_question) ? ' has-error' : '' }}">
            <label for="question{{ $n_question }}" class="col-md-2 col-form-label text-right">Question {{ $n_question }}</label>

            <div class="col-md-8">
                <textarea id="question{{ $n_question }}" class="form-control" name="question{{ $n_question }}" cols="60" rows="5"
                {{ $n_question>(isset($n_questions)?$n_questions:5)?'disabled':'' }}>{{ ${'question'.$n_question} ?? old('question'.$n_question) }}</textarea>

                @if ($errors->has('question'.$n_question))
                    <span class="help-block">
                        <strong>{{ $errors->first('question'.$n_question) }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="row form-group{{ $errors->has('solution'.$n_question) ? ' has-error' : '' }}">
            <label for="solution{{ $n_question }}" class="col-md-2 col-form-label text-right">Solution for question {{ $n_question }}</label>

            <div class="col-md-8">
                <textarea id="solution{{ $n_question }}" class="form-control" name="solution{{ $n_question }}" cols="60" rows="5"
                {{ $n_question>(isset($n_questions)?$n_questions:5)?'disabled':'' }}>{{ ${'solution'.$n_question} ?? old('solution'.$n_question) }}</textarea>

                @if ($errors->has('solution'.$n_question))
                    <span class="help-block">
                        <strong>{{ $errors->first('solution'.$n_question) }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="row form-group{{ $errors->has('hint'.$n_question) ? ' has-error' : '' }}">
            <label for="hint{{ $n_question }}" class="col-md-2 col-form-label text-right">Hint for question {{ $n_question }} (optional)</label>

            <div class="col-md-8">
                <textarea id="hint{{ $n_question }}" class="form-control" name="hint{{ $n_question }}" cols="60" rows="5"
                {{ $n_question>(isset($n_questions)?$n_questions:5)?'disabled':'' }}>{{ ${'hint'.$n_question} ?? old('hint'.$n_question) }}</textarea>

                @if ($errors->has('hint'.$n_question))
                    <span class="help-block">
                        <strong>{{ $errors->first('hint'.$n_question) }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
@endfor
</div>

<div class="row form-group">
    <div class="col-md-2 offset-md-8 text-right">
        <button id="add-question" type="button" class="btn btn-default btn-primary">
            Add
        </button>
        <button id="remove-question" type="button" class="btn btn-default btn-danger">
            Remove
        </button>
    </div>
</div>
