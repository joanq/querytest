@extends('layouts.app')

@section('content')
<div class="container justify-content-center">
    @include ('test.header_panel')
    <div class="row">
        <div class="col-md-12">
            <div class="card border-default">
                <div class="card-header">User:
                     <a href="{{ route('user.profile', ['user'=>$user->id]) }}">{{ $user->name }}</a>
                </div>
            </div>
            @foreach ($test->questions()->orderBy('id', 'asc')->get() as $question)
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                          <div class="col-md-2 text-right"><strong>Question {{ $loop->iteration }}</strong></div>
                          <div class="col-md-8">{!! Markdown::convertToHtml($question->question) !!}</div>
                        </div>
                        <div class="row">
                          <div class="col-md-2 text-right"><strong>Solution</strong></div>
                          <div class="col-md-8">
                            <div class="card bg-light mb-1">
                              <div class="card-body">
                                {!! Markdown::convertToHtml("```sql\n".$question->solution."\n```") !!}
                              </div>
                            </div>
                          </div>
                        </div>
                        @php
                            $attempts = $question->attempts()->where('user_id','=',$user->id)->orderBy('updated_at','desc')->get();
                        @endphp
                        @foreach ($attempts as $attempt)
                            <div class="row">
                              <div class="col-md-2 text-right">
                                  <strong class="{{ $attempt->message->type=='error'?'bg-danger':'bg-'.$attempt->message->type }}">{{ $attempt->message->code }}</strong>
                              </div>
                              <div class="col-md-8">
                                <div class="card bg-light mb-1">
                                  <div class="card-body">
                                    {!! Markdown::convertToHtml("```sql\n".$attempt->answer."\n```") !!}
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-2">{{ $attempt->updated_at }}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
