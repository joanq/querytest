@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border-primary mb-3">
                <div class="card-header text-white bg-primary">{{ $test->title }}</div>

                <div class="card-body">
                    Database: <strong>{{ $test->database }}</strong><br>
                    Questions: {{ count($questions) }}<br>
                    Description:<br>
                    {!! Markdown::convertToHtml($test->description ?? '') !!}<br>
                    @if (Auth::user()->hasRole('Teacher'))
                        <a class="btn btn-secondary" href="{{ route('test.attempts', ['test' => $test->id]) }}">View attempts</a>
                        <a class="btn btn-secondary" href="{{ route('test.summary', ['test' => $test->id]) }}">View summary</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @if (count($questions)>0)
        @foreach ($questions as $question)
            <div class="row" id="question{{ $question->id }}"><div class="col-md-12"><div class="card border-default">
                <div class="card-header">Question {{ $loop->iteration }}</div>
                <div class="card-body">
                    <div class="row"><div class="offset-md-2 col-md-8">{!! Markdown::convertToHtml($question->question) !!}</div></div>
                    @if ($test->isClosed(Auth::user()->group) == false)
                      @include ('question.answer_form')
                    @else
                      @include ('question.show_answer')
                    @endif
                </div>
            </div></div></div>
        @endforeach
    @else
        <div class="alert alert-warning">
            There aren't any questions.
        </div>
    @endif
</div>
@endsection

@section('scripts')
@parent
<script src="{{ asset('js/hint.js') }}"></script>
@endsection
