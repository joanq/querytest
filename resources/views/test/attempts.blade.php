@extends('layouts.app')

@section('content')
<div class="container">
    @include ('test.header_panel')
    @include ('test.attempts_panel')
</div>
@endsection
