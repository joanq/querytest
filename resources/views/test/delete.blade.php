@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border-danger">
                <div class="card-header text-white bg-danger">{{ $test->title }}</div>

                <div class="card-body">
                    Database: <strong>{{ $test->database }}</strong><br>
                    Questions: {{ count($test->questions) }}<br>
                    Description:<br>
                    {{ $test->description }}
                </div>
            </div>
        </div>
    </div>
    @if (count($test->questions)>0)
        @foreach ($test->questions as $question)
            <div class="row"><div class="col-md-12"><div class="card border-default">
                <div class="card-header">Question {{ $loop->iteration }}</div>
                <div class="card-body">
                    <div class="row"><div class="offset-md-2 col-md-8">{{ $question->question }}</div></div>
                </div>
            </div></div></div>
        @endforeach
    @else
        <div class="alert alert-warning">
            There aren't any questions.
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card border-danger">
                <div class="card-header text-white bg-danger">Confirm deletion</div>
                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('test.delete', ['test' => $test->id]) }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="col-md-2 offset-md-5">
                                <button type="submit" class="btn btn-danger btn-block">
                                    Confirm
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
