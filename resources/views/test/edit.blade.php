@extends('layouts.app')

@section('content')
<div class="container justify-content-center">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-primary">
                <div class="card-header text-white bg-primary">Edit Test</div>

                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('test.edit', ['test' => $testid]) }}">
                        @include('test.edit_or_create')
                        <div class="form-group">
                            <div class="col-md-10 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script src="{{ asset('js/create_test.js') }}"></script>
@stop
