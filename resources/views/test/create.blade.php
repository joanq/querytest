@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border-primary">
                <div class="card-header text-white bg-primary">Create Test</div>

                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('test.create') }}">
                        @include('test.edit_or_create', ['categories'=>$categories,'category'=>old('category')])
                        <div class="form-group">
                            <div class="col-md-10 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script src="{{ asset('js/create_test.js') }}"></script>
@stop
