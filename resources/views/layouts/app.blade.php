<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/querytest.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            @if (Auth::user()->isAdmin() === true)
                                @include ('navbar.admin')
                            @endif
                            @if (Auth::user()->hasRole('Teacher'))
                                @include ('navbar.teacher')
                            @endif
                            @include ('navbar.user')
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @include ('achievement')
            @include ('status')
            @yield('content')
        </main>
    </div>
    @section('scripts')
    <script src="{{ asset('js/app.js') }}"></script>
    @if (session('achievements'))
        <script type="text/javascript">
            $(window).on('load',function(){
                $(".toast").each(function(index) {
                    window.setTimeout(function(element) {
                        element.toast({delay: 5000});
                        element.toast('show');
                    }, 5500*index, $(this));
                });
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    @endif
    @show
</body>
</html>
