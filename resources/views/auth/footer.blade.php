
<footer>
    <div class="container">
        <div class="row mt-4 justify-content-center">
            <div class="col-md-8">
                <p>Aquest lloc utilitza cookies per mantenir la sessió. No es comparteixen les dades personals.</p>
                <p>El <strong>querytest</strong> és programari lliure sota els termes de la llicència <a href="https://www.gnu.org/licenses/agpl-3.0.en.html">GNU Affero General Public License</a>. Accedeix al <a href="https://gitlab.com/joanq/querytest">codi font</a>.</p>
                <p><small>&copy; 2023 Joan Queralt Molina</small></p>
          </div>
      </div>
    </div>
</footer>
