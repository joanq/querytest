@extends('layouts.app')

@section('content')
<div class="container">
  @forelse ($categories as $category)
      @php
          $tests = $category->getVisibleTests(Auth::user());
      @endphp
      @if ($tests->count()>0)
          <div class="row justify-content-center">
              <div class="col-md-12">
                  <div class="card border-primary mb-3">
                      @php
                          $expanded = true;
                          $show = "show";
                          $symbol = "\u{25BC}";
                          if ($category->isCollapsed(Auth::user())) {
                              $expanded = false;
                              $show = "";
                              $symbol = "\u{25B6}";
                          }
                      @endphp
                      <div class="card-header text-white bg-primary">
                        <button id="btn-collapse-{{ $category->id }}" class="btn" data-toggle="collapse" data-target="#collapse-{{ $category->id }}" aria-expanded="{{ $expanded }}" aria-controls="collapse-{{ $category->id }}">{{ $symbol }}</button>
                        {{ $category->name }}
                      </div>
                      <div id="collapse-{{ $category->id }}" class="collapse card-body {{ $show }}">
                          @foreach ($tests as $test)
                              @include ('test.index')
                          @endforeach
                      </div>
                  </div>
              </div>
          </div>
      @endif
  @empty
      <div class="alert alert-warning" role="alert">
          There aren't any tests yet.
      </div>
  @endforelse
</div>
@endsection

@section('scripts')
@parent
@if (Auth::user()->hasRole('Teacher'))
<script src="{{ asset('js/move_item.js') }}"></script>
@endif
<script src="{{ asset('js/collapse_category.js') }}"></script>
@endsection
