@foreach ($users as $user)
    @if ($user->id == Auth::user()->id)
        <tr class="table-success">
    @else
        <tr>
    @endif
    <td>{{ $user->position }}</td>
    @if (Auth::user()->hasRole('Teacher'))
        <td><a href="{{ route('user.profile', ['user'=>$user->id]) }}">{{ $user->name }}</a></td>
    @else
        <td>{{ $user->name }}</td>
    @endif
    <td>
    @if (!empty($user->group))
       {{ $user->group->name }}
    @endif
    </td>
    <td>{{ $user->points }}</td></tr>
@endforeach
