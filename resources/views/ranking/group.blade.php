<div class="card border-primary mb-3">
    <div class="card-header text-white bg-primary">Best users for group {{ $group->name }}</div>
    <div class="card-body">
        <table class="table table-striped table-sm">
            <tr><th>Position</th><th>Name</th><th>Group</th><th>Points</th></tr>
            <?php
            $bestUsers = \App\User::bestUsers($group);
            $nextUsers = null;
            if (!$bestUsers->contains('id', \Auth::user()->id) &&
                    isset(\Auth::user()->group) && \Auth::user()->group->name==$group->name) {
                $nextUsers = \Auth::user()->nextUsers($group);
            }
            ?>
            @include('ranking.users', ['users'=>$bestUsers])
            @isset($nextUsers)
                <tr><td colspan="4">...</td></tr>
                @include('ranking.users', ['users'=>$nextUsers])
            @endisset
        </table>
    </div>
</div>
