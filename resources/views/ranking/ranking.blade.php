@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-primary mb-3">
                <div class="card-header text-white bg-primary">Best overall users</div>
                <div class="card-body">
                    <table class="table table-striped table-sm">
                        <tr><th>Position</th><th>Name</th><th>Group</th><th>Points</th></tr>
                        <?php
                        $bestUsers = \App\User::bestUsers();
                        $nextUsers = null;
                        if (!$bestUsers->contains('id', \Auth::user()->id)) {
                            $nextUsers = \Auth::user()->nextUsers();
                        }
                        ?>
                        @include('ranking.users', ['users'=>$bestUsers])
                        @isset($nextUsers)
                            <tr><td colspan="4">...</td></tr>
                            @include('ranking.users', ['users'=>$nextUsers])
                        @endisset
                    </table>
                </div>
            </div>
        </div>
    </div>
    @if (\Auth::user()->hasRole('Teacher'))
        @include('group.filter')
        @if($group!='All')
            <div class="row">
                <div class="col-md-12">
                    @include('ranking.group', ['group'=>\App\Group::where('name',$group)->first()])
                </div>
            </div>
        @endif
    @else
        @isset(\Auth::user()->group)
            <div class="row">
                <div class="col-md-12">
                    @include('ranking.group', ['group'=>\Auth::user()->group])
                </div>
            </div>
        @endisset
    @endif
    <div class="row">
        <div class="col-md-12">
            @include('ranking.groups')
        </div>
    </div>
</div>
@endsection
