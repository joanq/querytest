<div class="card border-primary mb-3">
    <div class="card-header text-white bg-primary">Groups ranking</div>
    <div class="card-body">
        <table class="table table-striped table-sm">
            <tr><th>Position</th><th>Name</th><th>Average Points</th><th>Total Points</th></tr>
            <?php
                $groups = \App\Group::getRanking();
                $position = 1;
            ?>
            @foreach($groups as $gr)
                @if(isset(\Auth::user()->group) && \Auth::user()->group->id==$gr->id)
                    <tr class="table-success">
                @else
                    <tr>
                @endif
                    <td>{{ $position }}</td>
                    <td>{{ $gr->name }}</td>
                    <td>{{ $gr->avgPoints }}</td>
                    <td>{{ $gr->totalPoints }}</td>
                </tr>
                <?php $position++ ?>
            @endforeach
        </table>
    </div>
</div>
