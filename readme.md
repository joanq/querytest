# QueryTest

El QueryTest és un sistema d'aprenentatge de SQL semiautomatitzat.

Un professor de bases de dades no té temps de corregir totes les sentències
SQL que fan els seus alumnes. Si dóna la solució dels exercicis, pocs alumnes
dedicaran el temps necessari a comprovar, sentència per sentència, si els
resultats són exactament els mateixos i, si no ho són, exactament quin és
l'error que té.

El QueryTest facilita aquest procés i permet que els alumnes rebin una
retroacció immediata a les seves consultes, cosa que els permet adonar-se dels
errors i millorar-les.

## Funcionament bàsic

El funcionament és el següent:

- El professor crea un o més testos. Cada test pot tenir una colla de preguntes
i cada pregunta consta d'un enunciat i una sentència SQL solució.

- Els alumnes resolen les diverses preguntes enviant la seva proposta de
sentència SQL. El sistema executa la sentència solució i la sentència de
l'alumne i en compara els resultats.

- A partir d'aquesta comparació, el sistema retorna un missatge genèric a
l'alumne, indicant si els resultats coincideixen, o si no, quin tipus de
diferència hi ha.

- L'alumne pot modificar la seva sentència i tornar-la a enviar, fins que troba
la solució.

- En funció de la quantitat d'intents necessaris i altres paràmetres,
s'assigna una puntuació a cada alumne. Els alumnes poden comparar les seves
puntuacions com incentius per millorar.

## Comparació de resultats

Quan els resultats d'una sentència es comparen amb els resultats de la
sentència solució, el sistema retornarà un dels següents missatges:

- _Results match!_: els resultats coincideixen, sense tenir en compte l'ordre
de les columnes i el seu nom.

- _SQL error!_: la sentència no es pot executar perquè dóna un error.

- _Number of rows differ._: les dues sentències obtenen un nombre diferent de
resultats.

- _Number of columns differ._: les dues sentències obtenen el mateix nombre de
files, però no el mateix nombre de columnes.

- _Results doesn't match._: tot i que la quantitat de files i de columnes
coincideixen, les dades obtingudes no són les mateixes.

- _Bad ordering._: les dues sentències obtenen els mateixos resultats, però
en diferent ordre. L'ordre només es comprova en aquelles sentències on es
demani (és a dir, a la solució de les quals apareix una clàusula ORDER BY).

És important notar que el sistema no garanteix que les dues sentències SQL
siguin equivalents, només que retornen els mateixos resultats sobre la base de
dades en qüestió. Per tant, per crear bons testos, cal assegurar que a la base
de dades apareguin els casos límits necessaris.

En general, l'alumne tindrà la base de dades i podrà provar la seva sentència
abans d'enviar-la. Pot ser una bona idea que les dades concretes de la seva
versió i la versió del QueryTest siguin una mica diferents per garantir que
la sentència funciona bé en tots els casos.

## Puntuació

El sistema atorga punts per cada sentència resolta correctament, segons els
següents criteris:

- 10 punts menys 2 punts per intent addicional que s'hagi necessitat, fins a
un mínim de 4 punts.
- Per resoldre totes les preguntes d'un test s'atorguen 2 punts addicionals per
cada pregunta (per un test de 10 preguntes això serien 20 punts).

## Instal·lació

El QueryTest està programat utilitzant el framework Laravel. Les instruccions
oficials d'instal·lació són pertinents per la major part del procés
d'instal·lació. També pots seguir
[aquestes instruccions](https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades/tree/master/M6UF4/1-laravel).

Si vols instal·lar el QueryTest al teu servidor, segueix els següents passos:

1. Instal·la els prerequisits i el composer com en una instal·lació de Laravel.

2. Clona el projecte:

    `cd /var/www`

    `git clone https://gitlab.com/joanq/querytest.git`

3. Instal·la els components de Laravel necessaris:

    `cd querytest`

    `php ../composer.phar install`

4. Configura l'apache (o el nginx) com en una instal·lació de Laravel. En aquest
exemple, el _DocumentRoot_ seria `/var/www/querytest/public`.

5. Crea una base de dades en blanc i un usuari pel programa amb tots els permisos:

    `create database querytest;`

    `create user 'querytest'@'localhost' identified by 'una_contrasenya';`

    `grant all on querytest.* to 'querytest'@'localhost';`

6. Copia el fitxer `.env.example` a `.env`. Modifica `.env` indicant com a mínim
les dades d'accés a la base de dades i les dades de l'usuari administrador.

7. Genera una clau: `php artisan key:generate`.

8. Crea l'estructura de la base de dades i les dades per defecte:

    `php artisan migrate`

    `php artisan db:seed`

Després d'això ja hauries de poder entrar a l'entorn web amb l'usuari
administrador que hagis configurat i des d'allà crear testos i la resta
d'usuaris.

## Creació de testos

Els testos es poden crear des del propi entorn del QueryTest.

Els camps de descripció i els enunciats de cada pregunta s'interpreten en
Markdown, així que és fàcil donar-los format.

Naturalment, cal instal·lar al servidor les bases de dades sobre les quals
es volen crear testos, i donar a l'usuari `querytest` permís de consulta
(SELECT) sobre aquestes bases de dades.

## Llicència

El QueryTest és programari lliure sota els termes de la GNU Affero General
Public License (GNU AGPLv3).
