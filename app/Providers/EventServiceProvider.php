<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\TestCreated' => [
            'App\Listeners\CreateTestStatesOnNewTest'
        ],
        'App\Events\GroupCreated'=> [
            'App\Listeners\CreateTestStatesOnNewGroup'
        ],
        'App\Events\QuestionSolved' => [
            'App\Listeners\AwardPointsForQuestion',
            'App\Listeners\AchievementsForSolvingQuestion'
        ],
        'App\Events\QuestionFailed' => [
            'App\Listeners\AchievementsForFailingQuestion'
        ],
        'App\Events\TestSolved' => [
            'App\Listeners\AwardPointsForTest',
            'App\Listeners\AchievementsForSolvingTest'
        ],
        'App\Events\PointsChanged' => [
            'App\Listeners\AchievementsForPoints',
        ],
        'Assada\Achievements\Event\Unlocked' => [
            'App\Listeners\AchievementUnlocked'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
