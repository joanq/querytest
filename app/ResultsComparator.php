<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Database\QueryException;
use App\Test;
use App\Question;
use App\Message;
use App\Attempt;

class ResultsComparator {
    public static $TOL = 0.001;

    private $code;
    private $question;
    private $answer;
    private $expectedResults;
    private $userResults;

    private function configureConnection($database) {
        $config = App::make('config');
        $connections = $config->get('database.connections');
        $defaultConnection = $connections[$config->get('database.default')];
        $newConnection = $defaultConnection;
        $newConnection['database'] = $database;
        App::make('config')->set('database.connections.'.$database, $newConnection);
    }

    private function checkRows() {
        $done = false;
        if (count($this->expectedResults) != count($this->userResults)) {
            $this->code = 'ROWS_NUMBER';
            $done = true;
        }
        return $done;
    }

    private function checkCols() {
        $done = false;
        if (count($this->expectedResults)>0 && count($this->userResults)>0) {
            $expectedCols = count(get_object_vars($this->expectedResults[0]));
            $userCols = count(get_object_vars($this->userResults[0]));
            if ($expectedCols!=$userCols) {
                $this->code = 'COLS_NUMBER';
                $done = true;
            }
        }
        return $done;
    }

    private function getResults() {
        $results = [];
        foreach ($this->expectedResults as $expectedRow) {
            $expectedValues = array_values((array)$expectedRow);
            sort($expectedValues);
            $results[] = $expectedValues;
        }
        return $results;
    }

    private function compareArrays($array1, $array2) {
        $result = true;
        foreach ($array1 as $key => $value) {
            if (!$this->compareValues($value, $array2[$key])) {
                $result = false;
                break;
            }
        }
        return $result;
    }

    private function compareValues($val1, $val2) {
        $equals = false;
        if (is_numeric($val1) && is_numeric($val2)) {
            $equals = abs($val1-$val2)<ResultsComparator::$TOL;
        } else {
            $equals = $val1==$val2;
        }
        return $equals;
    }

    private function checkMatch() {
        $done = false;
        $results = $this->getResults();
        foreach ($this->userResults as $userRow) {
            $userValues = array_values((array)$userRow);
            sort($userValues);
            $found = false;
            $key = 0;
            foreach ($results as $key => $result) {
                if ($this->compareArrays($result, $userValues)) {
                    $found=true;
                    unset($results[$key]);
                    break;
                }
                $key++;
            }
            if ($found===false) {
                $this->code = 'NO_MATCH';
                $done = true;
                break;
            }
        }
        return $done;
    }

    private function checkOrder() {
        // Order is only important if the solution includes an order by clause
        if (stripos($this->question->solution, 'order by') === false) {
            return false;
        }
        // Order is not relevant if there are only 0 or 1 rows
        $results = $this->getResults();
        if (count($results) <= 1) {
            return false;
        }
        // If answer doesn't have an order by clause, order in not guaranted
        if (stripos($this->answer, 'order by') === false) {
            $this->code = 'ORDER';
            return true;
        }

        $done = false;
        for ($key=0; $key<count($results); $key++) {
            $userValues = array_values((array)$this->userResults[$key]);
            sort($userValues);
            $result = $results[$key];
            if (!$this->compareArrays($result, $userValues)) {
                $done = true;
                $this->code = 'ORDER';
                break;
            }
        }
        return $done;
    }

    private function compareResults() {
        $this->code = 'MATCH';
        $done = $this->checkRows();
        if (!$done) {
            $done = $this->checkCols();
        }
        if (!$done) {
            $done = $this->checkMatch();
        }
        if (!$done) {
            $done = $this->checkOrder();
        }
        $message = Message::where('code', $this->code)->firstOrFail();
        return $message;
    }

    private function checkSelect() {
        $test = $this->question->test;
        $solution = $this->question->solution;
        $this->expectedResults = DB::connection($test->database)->select(DB::raw($solution));
        try {
            DB::connection($test->database)->statement('SET @@MAX_STATEMENT_TIME=10.0');
            DB::connection($test->database)->statement('SET @@SQL_SELECT_LIMIT=?', [count($this->expectedResults)+1]);
            $this->userResults = DB::connection($test->database)->select(DB::raw($this->answer));
            $message = $this->compareResults();
        } catch (QueryException $e) {
            $message = Message::where('code', 'SQL_ERROR')->firstOrFail();
        } finally {
            DB::connection($test->database)->statement('SET @@MAX_STATEMENT_TIME=0');
            DB::connection($test->database)->statement('SET @@SQL_SELECT_LIMIT=DEFAULT');
        }
        return $message;
    }

    private function checkInsert() {
        $test = $this->question->test;
        $solution = $this->question->solution;
        $db = $test->database;
        // Find affected table
        $sol_array = explode(" ", $solution, 4);
        if (sizeof($sol_array)<3) {
            throw new QueryException();
        }
        $table = strtoupper($sol_array[1])=='INTO'?$sol_array[2]:$sol_array[1];
        // Run solution
        DB::connection($db)->beginTransaction();
        $solAffectedRows = DB::connection($db)->insert(DB::raw($solution));
        $this->expectedResults = DB::connection($db)->select(DB::raw("SELECT * FROM ".$table));
        DB::connection($db)->rollback();
        // Run answer
        try {
            DB::connection($db)->beginTransaction();
            $ansAffectedRows = DB::connection($db)->insert(DB::raw($this->answer));
            $this->userResults = DB::connection($db)->select(DB::raw("SELECT * FROM ".$table));
            DB::connection($db)->rollback();
            if ($solAffectedRows!=$ansAffectedRows) {
                $message = Message::where('code', 'ROWS_NUMBER')->firstOrFail();
            } else {
                $message = $this->compareResults();
            }
        } catch (QueryException $e) {
            $message = Message::where('code', 'SQL_ERROR')->firstOrFail();
            DB::connection($db)->rollback();
        }
        return $message;
    }

    private function checkDelete() {
        $test = $this->question->test;
        $solution = $this->question->solution;
        $db = $test->database;
        // Find affected table
        $sol_array = explode(" ", $solution, 4);
        if (sizeof($sol_array)<3) {
            throw new QueryException();
        }
        $table = $sol_array[2];
        // Run solution
        DB::connection($db)->beginTransaction();
        $solAffectedRows = DB::connection($db)->delete(DB::raw($solution));
        $this->expectedResults = DB::connection($db)->select(DB::raw("SELECT * FROM ".$table));
        DB::connection($db)->rollback();
        // Run answer
        try {
            DB::connection($db)->beginTransaction();
            $ansAffectedRows = DB::connection($db)->delete(DB::raw($this->answer));
            $this->userResults = DB::connection($db)->select(DB::raw("SELECT * FROM ".$table));
            DB::connection($db)->rollback();
            if ($solAffectedRows!=$ansAffectedRows) {
                $message = Message::where('code', 'ROWS_NUMBER')->firstOrFail();
            } else {
                $message = $this->compareResults();
            }
        } catch (QueryException $e) {
            $message = Message::where('code', 'SQL_ERROR')->firstOrFail();
            DB::connection($db)->rollback();
        }
        return $message;
    }

    private function checkUpdate() {
        $test = $this->question->test;
        $solution = $this->question->solution;
        $db = $test->database;
        // Find affected table
        $sol_array = explode(" ", $solution, 3);
        if (sizeof($sol_array)<2) {
            throw new QueryException();
        }
        $table = $sol_array[1];
        // Run solution
        DB::connection($db)->beginTransaction();
        $solAffectedRows = DB::connection($db)->update(DB::raw($solution));
        $this->expectedResults = DB::connection($db)->select(DB::raw("SELECT * FROM ".$table));
        DB::connection($db)->rollback();
        // Run answer
        try {
            DB::connection($db)->beginTransaction();
            $ansAffectedRows = DB::connection($db)->update(DB::raw($this->answer));
            $this->userResults = DB::connection($db)->select(DB::raw("SELECT * FROM ".$table));
            DB::connection($db)->rollback();
            if ($solAffectedRows!=$ansAffectedRows) {
                $message = Message::where('code', 'ROWS_NUMBER')->firstOrFail();
            } else {
                $message = $this->compareResults();
            }
        } catch (QueryException $e) {
            $message = Message::where('code', 'SQL_ERROR')->firstOrFail();
            DB::connection($db)->rollback();
        }
        return $message;
    }

    public function checkAnswer(Question $question, $answer) {
        $this->question = $question;
        $this->answer = $answer;
        $test = $question->test;
        $solution = $question->solution;
        $points = 0;
        $this->configureConnection($test->database);
        if (Str::startswith(strtoupper($solution), 'UPDATE')) {
            $message = $this->checkUpdate();
        } elseif (Str::startswith(strtoupper($solution), 'DELETE')) {
            $message = $this->checkDelete();
        } elseif (Str::startswith(strtoupper($solution), 'INSERT')) {
            $message = $this->checkInsert();
        } else {
            $message = $this->checkSelect();
        }
        return $message;
    }
}
