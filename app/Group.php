<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $dispatchesEvents = [
        'created' => \App\Events\GroupCreated::class,
    ];

    public function users() {
        return $this->hasMany('App\User');
    }

    public function testStates() {
        return $this-hasMany('App\TestState');
    }

    public static function getRanking() {
        return \DB::table('groups')->leftJoin('users','groups.id','=','users.group_id')->
            select('groups.id','groups.name')->selectRaw('sum(points) as totalPoints')->
            selectRaw('avg(points) as avgPoints')->groupBy('groups.id', 'groups.name')->
            orderBy('avgPoints', 'desc')->orderBy('totalPoints', 'desc')->get();
    }
}
