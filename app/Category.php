<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Category extends Model
{
    public function tests() {
        return $this->hasMany('App\Test');
    }

    public function users() {
        return $this->belongsToMany('App\User');
    }

    public function getVisibleTests(User $user) {
        $tests = $this->tests()->orderBy('tests.display_order', 'asc');
        if ($user->hasRole('Teacher')) {
            $tests = $tests->withCount('questions')->get();
        } else {
            $tests = $tests->withCount('questions')->
                join('test_states', 'tests.id', '=', 'test_states.test_id')->where([
                    ['test_states.published', '=', true],
                    ['test_states.group_id', '=', $user->group_id]
                ])->get();
        }
        return $tests;
    }

    public function isCollapsed(User $user) {
        return $this->users()->where('id', $user->id)->count()>0;
    }

    public function collapse(User $user) {
        $this->users()->attach($user->id);
    }

    public function expand(User $user) {
        $this->users()->detach($user->id);
    }
}
