<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserFailed500Attempts extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "500 Attempts Failed";

    /*
     * A small description for the achievement
     */
    public $description = "You've given a wrong answered 500 times";

    public $points = 500;

    public $icon = "images/500FailedAttempts.png";

    public function initializeUser($user) {
        $nSolved = $user->attempts()->join('messages', 'message_id', '=', 'messages.id')->where('type', '<>', 'SUCCESS')->count();
        $user->setProgress($this, $nSolved);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
