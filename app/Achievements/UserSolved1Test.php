<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserSolved1Test extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "One Complete Test";

    /*
     * A small description for the achievement
     */
    public $description = "You've solved your first test";

    public $points = 1;

    public $icon = "images/1TestSolved.png";

    public function initializeUser($user) {
        $points = 0;
        foreach ($user->testsSolved() as $test) {
            if ($test->questions()->count() == $test->nSolved) {
                $points++;
            }
        }
        $user->setProgress($this, $points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
