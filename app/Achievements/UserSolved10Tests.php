<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserSolved10Tests extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "10 Tests Completed";

    /*
     * A small description for the achievement
     */
    public $description = "You've solved 10 tests";

    public $points = 10;

    public $icon = "images/10TestsSolved.png";

    public function initializeUser($user) {
        $points = 0;
        foreach ($user->testsSolved() as $test) {
            if ($test->questions()->count() == $test->nSolved) {
                $points++;
            }
        }
        $user->setProgress($this, $points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
