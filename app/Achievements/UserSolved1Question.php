<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserSolved1Question extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "The First of Many";

    /*
     * A small description for the achievement
     */
    public $description = "You've solved your first question";

    public $points = 1;

    public $icon = "images/1QuestionSolved.png";

    public function initializeUser($user) {
        $nSolved = $user->testsSolved()->sum('nSolved');
        $user->setProgress($this, $nSolved);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
