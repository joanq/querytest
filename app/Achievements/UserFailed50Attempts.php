<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserFailed50Attempts extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "50 Attempts Failed";

    /*
     * A small description for the achievement
     */
    public $description = "You've given a wrong answered 50 times";

    public $points = 50;

    public $icon = "images/50FailedAttempts.png";

    public function initializeUser($user) {
        $nSolved = $user->attempts()->join('messages', 'message_id', '=', 'messages.id')->where('type', '<>', 'SUCCESS')->count();
        $user->setProgress($this, $nSolved);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
