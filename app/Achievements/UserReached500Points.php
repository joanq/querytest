<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserReached500Points extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "500 points";

    /*
     * A small description for the achievement
     */
    public $description = "You reached 500 points!";

    public $points = 500;

    public $icon = "images/500Points.png";

    public function initializeUser($user) {
        $user->setProgress($this, $user->points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
