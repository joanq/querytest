<?php

namespace App\Achievements;

use Illuminate\Support\Facades\DB;
use Assada\Achievements\Achievement;
use App\User;
use App\Question;

class UserFailed10TimesAndThenSolvedQuestion extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "Unquitter!";

    /*
     * A small description for the achievement
     */
    public $description = "Failed 10 times, then succeed";

    public $points = 1;

    public $icon = "images/10WrongAnswers.png";

    public static function questionSolved(User $user, Question $question) {
        $nAttempts = DB::table('attempts')->
          where([['attempts.user_id','=',$user->id],
            ['attempts.question_id','=',$question->id],])->count();
        if ($nAttempts>10) {
            $user->addProgress(new UserFailed10TimesAndThenSolvedQuestion(), 1);
        }
    }

    public function initializeUser($user) {
        $points = DB::select("select count(*) as p from (
          select question_id from attempts a
          where question_id in (
            select question_id from attempts a
            join messages m on a.message_id=m.id
            where user_id = $user->id
            and m.type = 'success'
          ) and user_id = $user->id
          group by question_id
          having count(*)>10
        ) as t")[0]->p;
        $user->setProgress($this, $points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
