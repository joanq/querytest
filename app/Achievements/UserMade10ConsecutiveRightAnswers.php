<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;

use App\User;
use Illuminate\Support\Facades\DB;

class UserMade10ConsecutiveRightAnswers extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "10 Consecutive Right Answers";

    /*
     * A small description for the achievement
     */
    public $description = "You have given 10 right answers in a row!";

    public $points = 10;

    public $icon = "images/10ConsecutiveRightAnswers.png";

    public function initializeUser(User $user) {
        // Check if user has already unlocked this achievement
        $maxRightAnswers = DB::select(DB::raw(
            "select max(ra) as maxRightAnswers
            from (
                select @rightAnswers:=IF(message_id=1, @rightAnswers+1, 0) as ra
                from attempts a, (select @rightAnswers:=0) var_init_subquery
                where user_id=:userId
                order by id) t"
        ), array('userId'=>$user->id))[0]->maxRightAnswers;
        if ($maxRightAnswers>=$this->points) {
            $user->setProgress($this, $maxRightAnswers);
        }
        // Find current status
        $result = DB::select(DB::raw(
            "select ra as currentRightAnswers
            from (
                select id, @rightAnswers:=IF(message_id=1, @rightAnswers+1, 0) as ra
                from attempts a, (select @rightAnswersr:=0) var_init_subquery
                where user_id=:userId
                order by id) t
            order by id desc limit 1"
        ), array('userId'=>$user->id));
        if (sizeof($result)>0) {
            $user->setProgress($this, $result[0]->currentRightAnswers);
        }
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
