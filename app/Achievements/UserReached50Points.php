<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserReached50Points extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "50 points";

    /*
     * A small description for the achievement
     */
    public $description = "You reached 50 points!";

    public $points = 50;

    public $icon = "images/50Points.png";

    public function initializeUser($user) {
        $user->setProgress($this, $user->points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
