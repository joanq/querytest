<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserReached1500Points extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "1500 points";

    /*
     * A small description for the achievement
     */
    public $description = "You reached 1500 points!";

    public $points = 1500;

    public $icon = "images/1500Points.png";

    public function initializeUser($user) {
        $user->setProgress($this, $user->points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
