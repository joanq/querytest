<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserSolved3Tests extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "3 Tests Completed";

    /*
     * A small description for the achievement
     */
    public $description = "You've solved 3 tests";

    public $points = 3;

    public $icon = "images/3TestsSolved.png";

    public function initializeUser($user) {
        $points = 0;
        foreach ($user->testsSolved() as $test) {
            if ($test->questions()->count() == $test->nSolved) {
                $points++;
            }
        }
        $user->setProgress($this, $points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
