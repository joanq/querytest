<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserReached1000Points extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "1000 points";

    /*
     * A small description for the achievement
     */
    public $description = "You reached 1000 points!";

    public $points = 1000;

    public $icon = "images/1000Points.png";

    public function initializeUser($user) {
        $user->setProgress($this, $user->points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
