<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;

use App\User;
use App\Question;
use Illuminate\Support\Facades\DB;

class UserSolved100QuestionsAtFirstAttempt extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "100 questions solved at first attempt";

    /*
     * A small description for the achievement
     */
    public $description = "You have solved 100 questions at first attempt";

    public $points = 100;

    public $icon = "images/100QuestionsSolvedAtFirstAttempt.png";

    public static function questionSolved(User $user, Question $question) {
        $nAttempts = DB::table('attempts')->
          where([['attempts.user_id','=',$user->id],
            ['attempts.question_id','=',$question->id],])->count();
        if ($nAttempts==1) {
            $user->addProgress(new UserSolved100QuestionsAtFirstAttempt(), 1);
        }
    }

    public function initializeUser(User $user) {
        $nSolved = DB::table('questions')->
            join('attempts', 'questions.id', '=', 'attempts.question_id')->
            join('messages', 'attempts.message_id', '=', 'messages.id')->
            where('attempts.user_id','=',$user->id)->
            groupBy('questions.id')->
            havingRaw("count(*)=1 and count(case when messages.type='success' then 0 end)>0")->
            selectRaw('questions.id')->get()->count();
        $user->setProgress($this, $nSolved);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
