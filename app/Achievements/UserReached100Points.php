<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserReached100Points extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "100 points";

    /*
     * A small description for the achievement
     */
    public $description = "You reached 100 points!";

    public $points = 100;

    public $icon = "images/100Points.png";

    public function initializeUser($user) {
        $user->setProgress($this, $user->points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
