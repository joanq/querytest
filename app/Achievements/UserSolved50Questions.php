<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserSolved50Questions extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "50 Questions Solved";

    /*
     * A small description for the achievement
     */
    public $description = "You've solved 50 questions";

    public $points = 50;

    public $icon = "images/50QuestionsSolved.png";

    public function initializeUser($user) {
        $nSolved = $user->testsSolved()->sum('nSolved');
        $user->setProgress($this, $nSolved);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
