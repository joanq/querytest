<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserSolved5Tests extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "5 Tests Completed";

    /*
     * A small description for the achievement
     */
    public $description = "You've solved 5 tests";

    public $points = 5;

    public $icon = "images/5TestsSolved.png";

    public function initializeUser($user) {
        $points = 0;
        foreach ($user->testsSolved() as $test) {
            if ($test->questions()->count() == $test->nSolved) {
                $points++;
            }
        }
        $user->setProgress($this, $points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
