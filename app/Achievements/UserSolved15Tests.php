<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserSolved15Tests extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "15 Tests Completed";

    /*
     * A small description for the achievement
     */
    public $description = "You've solved 15 tests";

    public $points = 15;

    public $icon = "images/15TestsSolved.png";

    public function initializeUser($user) {
        $points = 0;
        foreach ($user->testsSolved() as $test) {
            if ($test->questions()->count() == $test->nSolved) {
                $points++;
            }
        }
        $user->setProgress($this, $points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
