<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserSolved500Questions extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "500 Questions Solved";

    /*
     * A small description for the achievement
     */
    public $description = "You have solved 500 questions";

    public $points = 500;

    public $icon = "images/500QuestionsSolved.png";

    public function initializeUser($user) {
        $nSolved = $user->testsSolved()->sum('nSolved');
        $user->setProgress($this, $nSolved);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
