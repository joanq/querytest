<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserReached200Points extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "200 points";

    /*
     * A small description for the achievement
     */
    public $description = "You reached 200 points!";

    public $points = 200;

    public $icon = "images/200Points.png";

    public function initializeUser($user) {
        $user->setProgress($this, $user->points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
