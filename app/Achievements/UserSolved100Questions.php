<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserSolved100Questions extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "A Hundred Questions Solved";

    /*
     * A small description for the achievement
     */
    public $description = "You have solved a hundred questions";

    public $points = 100;

    public $icon = "images/100QuestionsSolved.png";

    public function initializeUser($user) {
        $nSolved = $user->testsSolved()->sum('nSolved');
        $user->setProgress($this, $nSolved);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
