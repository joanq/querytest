<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserSolved200Questions extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "Two Hundred Questions Solved";

    /*
     * A small description for the achievement
     */
    public $description = "You have solved 200 questions";

    public $points = 200;

    public $icon = "images/200QuestionsSolved.png";

    public function initializeUser($user) {
        $nSolved = $user->testsSolved()->sum('nSolved');
        $user->setProgress($this, $nSolved);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
