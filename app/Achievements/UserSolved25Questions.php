<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserSolved25Questions extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "25 Questions Solved";

    /*
     * A small description for the achievement
     */
    public $description = "You've solved 25 questions";

    public $points = 25;

    public $icon = "images/25QuestionsSolved.png";

    public function initializeUser($user) {
        $nSolved = $user->testsSolved()->sum('nSolved');
        $user->setProgress($this, $nSolved);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
