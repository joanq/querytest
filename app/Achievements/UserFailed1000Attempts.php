<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserFailed1000Attempts extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "1000 Attempts Failed";

    /*
     * A small description for the achievement
     */
    public $description = "You've given a wrong answered 1000 times";

    public $points = 1000;

    public $icon = "images/1000FailedAttempts.png";

    public function initializeUser($user) {
        $nSolved = $user->attempts()->join('messages', 'message_id', '=', 'messages.id')->where('type', '<>', 'SUCCESS')->count();
        $user->setProgress($this, $nSolved);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
