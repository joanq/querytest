<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserFailed100Attempts extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "100 Attempts Failed";

    /*
     * A small description for the achievement
     */
    public $description = "You've given a wrong answer 100 times";

    public $points = 100;

    public $icon = "images/100FailedAttempts.png";

    public function initializeUser($user) {
        $nSolved = $user->attempts()->join('messages', 'message_id', '=', 'messages.id')->where('type', '<>', 'SUCCESS')->count();
        $user->setProgress($this, $nSolved);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
