<?php

namespace App\Achievements;

use Assada\Achievements\Achievement;
use App\User;

class UserReached2000Points extends Achievement
{
    /*
     * The achievement name
     */
    public $name = "2000 points";

    /*
     * A small description for the achievement
     */
    public $description = "You reached 2000 points!";

    public $points = 2000;

    public $icon = "images/2000Points.png";

    public function initializeUser($user) {
        $user->setProgress($this, $user->points);
    }

    public function initialize() {
        foreach (User::all() as $user) {
            $this->initializeUser($user);
        }
    }
}
