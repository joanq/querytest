<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Assada\Achievements\Achiever;
use Illuminate\Support\Facades\DB;
use App\Events\PointsChanged;

class User extends Authenticatable
{
    use Notifiable;
    use Achiever;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'last_login_at', 'last_login_ip',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getNameAttribute() {
        return $this->firstname.' '.$this->lastname;
    }

    public function setPointsAttribute($points) {
        $oldPoints = $this->points;
        $this->attributes['points']=$points;
        event(new PointsChanged($this, $oldPoints));
    }

    public function tests() {
        return $this->hasMany('App\Test');
    }

    public function attempts() {
        return $this->hasMany('App\Attempt');
    }

    public function roles() {
        return $this->belongsToMany('App\Role');
    }

    public function group() {
        return $this->belongsTo('App\Group');
    }

    public function categories() {
        return $this->belongsToMany('App\Category');
    }

    public function hasRole($roleName) {
        return $this->roles()->where('name', $roleName)->count()>0;
    }

    public function addRole($roleName) {
        if ($this->hasRole($roleName) == false) {
            $role = Role::where('name', $roleName)->first();
            $this->roles()->attach($role->id);
        }
    }

    public function removeRole($roleName) {
        if ($this->hasRole($roleName)) {
            $role = Role::where('name', $roleName)->first();
            $this->roles()->detach($role->id);
        }
    }

    public function isAdmin() {
        return $this->hasRole('Admin');
    }

    public function getPosition(Group $group=null) {
        if ($group==null) {
            $position = User::where('points', '>', $this->points)->count()+1;
        } else {
            $position = User::where([['group_id','=',$group->id],['points', '>', $this->points]])->count()+1;
        }
        return $position;
    }

    public static function bestUsers(Group $group=null) {
        if ($group==null) {
            $bestUsers = User::orderBy('points', 'desc')->take(10)->with('group')->get();
        } else {
            $bestUsers = User::where('group_id', $group->id)->orderBy('points', 'desc')->take(10)->with('group')->get();
        }
        foreach ($bestUsers as $user) {
            $user->position = $user->getPosition($group);
        }
        return $bestUsers;
    }

    public function nextUsers(Group $group=null) {
        if ($group==null) {
            $nextUsers = User::where('points', '>', $this->points)->
                orderBy('points', 'asc')->take(2)->with('group')->get();
        } else {
            $nextUsers = User::where([['group_id','=',$group->id],['points', '>', $this->points]])->
                orderBy('points', 'asc')->take(2)->with('group')->get();
        }
        $nextUsers = $nextUsers->reverse();
        foreach ($nextUsers as $nextUser) {
            $nextUser->position = $nextUser->getPosition($group);
        }
        $nextUsers->push($this);
        $this->position = $this->getPosition($group);
        return $nextUsers;
    }

    public function testsSolved() {
        $tests = Test::withCount('questions')->get();
        $solved = DB::table('tests')->
            join('questions', 'tests.id', '=', 'questions.test_id')->
            join('attempts', 'questions.id', '=', 'attempts.question_id')->
            join('messages', 'attempts.message_id', '=', 'messages.id')->
            where([['attempts.user_id','=',$this->id], ['messages.type','=','success']])->
            groupBy('tests.id')->
            select('tests.id')->selectRaw('count(*) as nSolved')->
            get();
        $tests = $tests->reject(function ($test) {
            return $test->areResultsHidden($this->group);
        });
        foreach ($tests as $test) {
            $test->nSolved = 0;
            $testSolved = $solved->firstWhere('id', $test->id);
            if (isset($testSolved)) {
                $test->nSolved = $testSolved->nSolved;
            }
        }
        $tests = $tests->reject(function ($test) {
            return $test->nSolved==0;
        });
        return $tests;
    }
}
