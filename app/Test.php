<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Test extends Model
{
    protected $dispatchesEvents = [
        'created' => \App\Events\TestCreated::class,
    ];

    public function questions() {
        return $this->hasMany('App\Question');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function attempts() {
        return $this->hasManyThrough('App\Attempt', 'App\Question');
    }

    public function testStates() {
        return $this->hasMany('App\TestState');
    }

    public function testState(Group $group=null) {
        $state = null;
        if (isset($group)) {
            $state = $this->testStates()->where('group_id', $group->id)->first();
            if (!isset($state)) {
                $state = new TestState();
                $state->group_id = $group->id;
                $state->test_id = $this->id;
                $state->save();
            }
        }
        return $state;
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function isPublished(Group $group=null) {
        $published = false;
        $state = $this->testState($group);
        if (isset($state)) {
            $published = $state->published;
        }
        return $published;
    }

    public function isClosed(Group $group=null) {
        $closed= false;
        $now = Carbon::now();
        $state = $this->testState($group);
        if (isset($state)) {
            $openFrom = $state->open_from;
            $openUntil = $state->open_until;
            if (($openFrom == null || $openFrom->greaterThan($now)) || ($openUntil != null && $openUntil->lessThanOrEqualTo($now))) {
                $closed = true;
            }
        }
        return $closed;
    }

    public function isOpen(Group $group=null) {
        return !$this->isClosed($group);
    }

    public function areResultsHidden(Group $group=null) {
        $hidden = false;
        $state = $this->testState($group);
        if (isset($state)) {
            $hidden = $state->hide_results;
        }
        return $hidden;
    }

    public function areResultsShown(Group $group=null) {
        return !$this->areResultsHidden($group);
    }

    public function setPublished(Group $group, $published=true) {
        $state = $this->testState($group);
        $state->published = $published;
        $state->save();
    }

    public function setClosed(Group $group, $closed=true) {
        $state = $this->testState($group);
        $state->closed = $closed;
        $state->save();
    }

    public function setHideResults(Group $group, $hideResults=true) {
        $state = $this->testState($group);
        $state->hide_results = $hideResults;
        $state->save();
    }

    public function nQuestionsSolved(User $user) {
        $matchId = Message::where('code', 'MATCH')->first()->id;
        $questionsSolved = $this->attempts()->where([
            ['user_id', $user->id],
            ['message_id', $matchId]
        ])->count();
        return $questionsSolved;
    }

    public function openFrom(Group $group=null) {
        $open = false;
        $state = $this->testState($group);
        if (isset($state)) {
            $open = $state->open_from;
        }
        return $open;
    }

    public function openUntil(Group $group=null) {
        $open = false;
        $state = $this->testState($group);
        if (isset($state)) {
            $open = $state->open_until;
        }
        return $open;
    }

    public function setOpenFrom(Group $group, Carbon $datetime) {
        $state = $this->testState($group);
        $state->open_from = $datetime;
        $state->save();
    }

    public function setOpenUntil(Group $group, Carbon $datetime) {
        $state = $this->testState($group);
        $state->open_until = $datetime;
        $state->save();
    }

    public function clone() {
        $newtest = new Test();
        $newtest->title = $this->title.' (copy at '.Carbon::now().')';
        $newtest->description = $this->description;
        $newtest->database = $this->database;
        $newtest->category_id = $this->category_id;
        $newtest->user_id = \Auth::user()->id;
        \Auth::user()->tests()->save($newtest);
        $newtest->display_order = $newtest->id;
        $newtest->save();
        foreach ($this->questions as $question) {
            $newquestion = $question->clone();
            $newtest->questions()->save($newquestion);
        }
        return $newtest;
    }
}
