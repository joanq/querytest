<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Group;
use App\Role;
use App\Test;

class UserController extends Controller
{
    private static $pagination = 30;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Admin')->except('showProfile');
        $this->middleware('role.or.user:Teacher')->only('showProfile');
    }

    public function setOrderBy(Request $request) {
        $orderby = $request->input('orderby', 'lastname');
        if (!in_array($orderby, ['lastname','firstname','email','group','points']))
            $orderby='lastname';
        $request->session()->put('usersorderby', $orderby);
        $dir = $request->input('dir', 'asc');
        if (!in_array($dir, ['asc', 'desc']))
            $dir='asc';
        $request->session()->put('usersdir', $dir);
        return redirect()->back();
    }

    public function showList(Request $request) {
        $orderby=$request->session()->get('usersorderby','lastname');
        $dir=$request->session()->get('usersdir','asc');
        $group=$request->session()->get('group','All');
        $users = User::select('users.*')->leftJoin('groups', 'group_id', 'groups.id')->with('group');
        if ($group!='All') {
            $users = $users->where('groups.name', $group);
        }
        switch ($orderby) {
            case 'lastname':
                $users = $users->orderBy('lastname', $dir)->orderBy('firstname', $dir);
            case 'firstname':
                $users = $users->orderBy('firstname', $dir)->orderBy('lastname', $dir);
            case 'email':
                $users = $users->orderBy('email', $dir);
            case 'group':
                $users = $users->orderBy('groups.name', $dir)->orderBy('lastname')->orderBy('firstname');
            case 'points':
                $users = $users->orderBy('points', $dir)->orderBy('lastname')->orderBy('firstname');
            break;
        }
        $users = $users->paginate(UserController::$pagination);
        $groups = Group::all();
        $icon = $dir=='asc'?'&#8643;':'&#8638;';
        $dir=($dir=='asc'?'desc':'asc');
        return view('user.list')->with(['users'=>$users, 'group'=>$group, 'groups'=>$groups, 'orderby'=>$orderby, 'dir'=>$dir, 'icon'=>$icon]);
    }

    public function showEditForm(Request $request, User $user) {
        return view('user.edit')->with('user', $user);
    }

    public function editForm(Request $request, User $user) {
        $validatedData = $request->validate([
            'firstname' => 'required|min:2|string|max:255',
            'lastname' => 'nullable|string|max:255',
            'email' => 'required|email|unique:users,email,'.$user->email.',email',
            'password' => 'nullable|min:6|string',
            'group' => 'present|string|nullable|notIn:All|exists:groups,name',
            'points' => 'integer|min:0'
        ]);
        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        if ($request->input('teacher')!=null) {
            $user->addRole('Teacher');
        } else {
            $user->removeRole('Teacher');
        }
        if ($request->input('admin')!=null) {
            $user->addRole('Admin');
        } else {
            $user->removeRole('Admin');
        }
        if ($request->input('password')!=null) {
            $user->password = bcrypt($request->input('password'));
        }
        if (null!==$request->input('group') && $request->input('group')!=='') {
            $group = Group::where('name', $request->input('group'))->first();
            if ($group===null) {
                $user->group()->dissociate();
            } else {
                $user->group()->associate($group);
            }
        } else {
            $user->group()->dissociate();
        }
        $user->save();
        if ($request->input('points')!=null && $request->input('points')!=$user->points) {
            $user->points = $request->input('points');
        }
        $user->save();
        $request->session()->flash('status', 'User saved!');
        return redirect()->route('user.list');
    }

    public function showDeleteForm(Request $request, User $user) {
        return view('user.delete')->with('user', $user);
    }

    public function deleteUser(Request $request, User $user) {
        $user->delete();
        $request->session()->flash('status', 'User deleted!');
        return redirect()->route('user.list');
    }

    public function showCreateForm() {
        return view('user.create');
    }

    public function showFromCSVForm() {
        return view('user.fromcsv');
    }

    public function createForm(Request $request) {
        $createdUsers=0;
        $input = $request->all();
        DB::beginTransaction();
        try {
            for ($nUser=1; $nUser<=12; $nUser++) {
                $rules = [];
                $rules['firstname'.$nUser]='nullable|min:2|string|max:255';
                $rules['lastname'.$nUser]='nullable|min:2|string|max:255';
                $rules['email'.$nUser]='nullable|email|unique:users,email|required_with:name'.$nUser;
                $rules['password'.$nUser]='nullable|min:6|string|required_with:name'.$nUser;
                $rules['group'.$nUser]='present|string|nullable|notIn:All|exists:groups,name';
                $request->validate($rules);
                if ($input['firstname'.$nUser]!=null && $input['email'.$nUser]!=null &&
                        $input['password'.$nUser]!=null) {
                    $user = new User();
                    $user->firstname = $input['firstname'.$nUser];
                    $user->lastname = $input['lastname'.$nUser];
                    $user->email = $input['email'.$nUser];
                    $user->password = bcrypt($input['password'.$nUser]);
                    $group = Group::where('name', $input['group'.$nUser])->first();
                    if ($group===null) {
                        $user->group()->dissociate();
                    } else {
                        $user->group()->associate($group);
                    }
                    $user->save();
                    if (isset($input['admin'.$nUser])) {
                        $user->addRole('Admin');
                    }
                    if (isset($input['teacher'.$nUser])) {
                        $user->addRole('Teacher');
                    }
                    $createdUsers++;
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();
        $request->session()->flash('status', $createdUsers.' users created.');
        return redirect()->route('user.create');
    }

    public function createFromCSV(Request $request) {
        $rules = [];
        $rules['csvfile']='required|file';
        $rules['delimiter']='present|string|max:255|min:1';
        $rules['enclousure']='present|string|max:255';
        $rules['password']='min:6|string|required_with:name';
        $rules['group']='present|string|nullable|notIn:All|exists:groups,name';
        $request->validate($rules);

        $file = $request->csvfile;
        $skip = $request->input('skip-first-line')!=null;
        $delimiter = $request->delimiter==null?'':$request->delimiter;
        $enclousure = $request->enclousure==null?'':$request->enclousure;
        $group = Group::where('name', $request->group)->first();
        $rules = [];
        $rules['firstname']='min:2|string|max:255';
        $rules['lastname']='min:2|string|max:255';
        $rules['email']='email|unique:users,email|required_with:name';
        $status = new \Illuminate\Support\MessageBag;
        $handle = fopen($file->path(), 'r');
        $n_users=0;
        $line=1;
        if ($handle !== false) {
            while (($data = fgetcsv($handle, 1000, $delimiter, $enclousure)) !== FALSE) {
                if ($skip==false) {
                    if (sizeof($data)>=3) {
                        $request->merge(['firstname' => $data[0]]);
                        $request->merge(['lastname' => $data[1]]);
                        $request->merge(['email' => $data[2]]);
                        $request->validate($rules);
                        $user = new User();
                        $user->firstname = $request->firstname;
                        $user->lastname = $request->lastname;
                        $user->email = $request->email;
                        $user->password = bcrypt($request->password);
                        if ($group!==null) {
                            $user->group()->associate($group);
                        }
                        $user->save();
                        $n_users++;
                        $status->add($line, 'User '.$user->name.' ('.$user->email.') created.');
                        $request->session()->flash('status', $status);
                    }
                } else {
                    $skip = false;
                }
                $line++;
            }
        }
        return redirect()->route('user.fromcsv');
    }

    public function showProfile(Request $request, User $user) {
        $group = $user->group;
        $tests = Test::select('tests.*')->join('categories', 'tests.category_id', '=', 'categories.id')
            ->orderBy('categories.display_order', 'asc')
            ->orderBy('tests.display_order', 'asc')->get();
        $tests=$tests->filter(function ($test) use ($group) {
            return $test->isPublished($group);
        });
        foreach ($tests as $test) {
            $questions = \DB::table('questions')
                ->select('questions.id')
                ->where('questions.test_id', '=', $test->id)
                ->orderBy('id', 'asc')
                ->get();
            $results = array();
            foreach ($questions as $question) {
                $result = \DB::table('attempts')
                    ->where([['attempts.user_id', '=', $user->id],
                        ['attempts.question_id', '=', $question->id]])
                    ->join('messages', 'attempts.message_id', '=', 'messages.id')
                    ->selectRaw('max(case messages.type when "success" then 2 else 1 end) as message')
                    ->first()->message;
                if ($result!='') {
                    if ($test->areResultsShown($user->group)) {
                        $results[] = $result;
                    } else {
                        $results[] = 3;
                    }
                } else {
                    $results[] = 0;
                }
            }
            $test->results = $results;
        }
        return view('user.profile')->with(['user'=>$user, 'tests'=>$tests]);
    }
}
