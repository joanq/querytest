<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\Question;
use App\Group;
use App\Category;
use Carbon\Carbon;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:Teacher']);
    }

    public function showCreateForm() {
        $categories = Category::orderBy('display_order', 'asc')->get();
        return view('test.create')->with('categories', $categories);
    }

    public function showEditForm(Request $request, Test $test) {
        $data = [];
        $data['testid']=$test->id;
        $data['title']=$test->title;
        $data['description']=$test->description;
        $data['database']=$test->database;
        $questions = $test->questions()->orderBy('id', 'asc')->get();
        $data['n_questions']=$questions->count();
        $n_question = 1;
        foreach ($questions as $question) {
            $data["question$n_question"] = $question->question;
            $data["solution$n_question"] = $question->solution;
            $data["hint$n_question"] = $question->hint;
            $n_question++;
        }
        $data['category'] = $test->category->name;
        $data['categories'] = Category::orderBy('display_order', 'asc')->get();
        return view('test.edit')->with($data);
    }

    public function showDeleteForm(Test $test) {
        return view('test.delete')->with('test', $test);
    }

    public function showVisibilityForm(Test $test) {
        $groups = Group::all();
        return view('test.visibility')->with(['test'=>$test, 'groups'=>$groups]);
    }

    public function visibilityForm(Request $request, Test $test) {
        $groups = Group::all();
        $published = $request->published!=null?$request->published:[];
        $hide_results = $request->hide_results!=null?$request->hide_results:[];
        for ($i=0; $i<sizeof($groups); $i++) {
            $group = $groups[$i];
            $test->setPublished($group, in_array($group->id, $published));
            $test->setHideResults($group, in_array($group->id, $hide_results));

            if ($request->open_from_date[$i] != null || $request->open_from_time[$i] != null) {
                $open_from_date = $request->open_from_date[$i]?$request->open_from_date[$i]:Carbon::now()->format('Y-m-d');
                $open_from_time = $request->open_from_time[$i]?$request->open_from_time[$i]:'00:00';
                $open_from = Carbon::createFromFormat('Y-m-d H:i', $open_from_date.' '.$open_from_time);
                $test->setOpenFrom($group, $open_from);
            }
            if ($request->open_until_date[$i] != null || $request->open_until_time[$i] != null) {
                $open_until_date = $request->open_until_date[$i]?$request->open_until_date[$i]:Carbon::now()->format('Y-m-d');
                $open_until_time = $request->open_until_time[$i]?$request->open_until_time[$i]:'00:00';
                $open_until = Carbon::createFromFormat('Y-m-d H:i', $open_until_date.' '.$open_until_time);
                $test->setOpenUntil($group, $open_until);
            }
        }
        $request->session()->flash('status', 'Visibility saved!');
        return redirect('home');
    }

    public function createForm(Request $request) {
        $validatedData = $request->validate([
            'title' => 'required|string|max:255|unique:tests',
            'description' => 'nullable|string|max:65535',
            'database' => 'required|string|max:255',
            'category' => 'required|exists:categories,id'
        ]);
        $test = new Test();
        $test->title = $request->input('title');
        $test->description = $request->input('description');
        $test->database = $request->input('database');
        $test->category_id = $request->input('category');
        \Auth::user()->tests()->save($test);
        $test->display_order = $test->id;
        $test->save();

        $n_question=1;
        do {
            $question = new Question();
            $question->solution = is_null($request->input('solution'.$n_question))?
                '':$request->input('solution'.$n_question);
            $question->question = is_null($request->input('question'.$n_question))?
                '':$request->input('question'.$n_question);
            $question->hint = is_null($request->input('hint'.$n_question))?
                '':$request->input('hint'.$n_question);
            if ($question->solution!='' || $question->question!='') {
                $test->questions()->save($question);
            }
            $n_question++;
        } while (($question->solution!='' || $question->question!='') && $n_question<100);

        $request->session()->flash('status', 'Test saved!');
        return redirect('home');
    }

    public function editForm(Request $request, Test $test) {
      $validatedData = $request->validate([
          'title' => 'required|string|max:255|unique:tests,title,'.$test->id,
          'description' => 'nullable|string|max:65535',
          'database' => 'required|string|max:255',
          'category' => 'required|exists:categories,id'
      ]);
      $test->title = $request->input('title');
      $test->description = $request->input('description');
      $test->database = $request->input('database');
      $test->category_id = $request->input('category');
      $test->save();

      $n_question=1;
      foreach ($test->questions as $question) {
          if ($request->input('solution'.$n_question)!=null ||
              $request->input('question'.$n_question)!=null) {
              $question->solution = is_null($request->input('solution'.$n_question))?
                  '':$request->input('solution'.$n_question);
              $question->question = is_null($request->input('question'.$n_question))?
                  '':$request->input('question'.$n_question);
              $question->hint = is_null($request->input('hint'.$n_question))?
                  '':$request->input('hint'.$n_question);
              $question->save();
          } else {
              $question->delete();
          }
          $n_question++;
      }
      do {
          $question = new Question();
          $question->solution = is_null($request->input('solution'.$n_question))?
              '':$request->input('solution'.$n_question);
          $question->question = is_null($request->input('question'.$n_question))?
              '':$request->input('question'.$n_question);
          $question->hint = is_null($request->input('hint'.$n_question))?
              '':$request->input('hint'.$n_question);
          if ($question->solution!='' || $question->question!='') {
              $test->questions()->save($question);
          }
          $n_question++;
      } while (($question->solution!='' || $question->question!='') && $n_question<100);

      $request->session()->flash('status', 'Test saved!');
      return redirect('home');
    }

    public function deleteForm(Request $request, Test $test) {
        $test->delete();
        $request->session()->flash('status', 'Test deleted!');
        return redirect('home');
    }

    public function clone(Request $request, Test $test) {
        $test->clone();
        $request->session()->flash('status', 'Test cloned!');
        return redirect('home');
    }
}
