<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Response;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Teacher')->except('collapse','expand');
    }

    public function showManageForm(Request $request) {
        $categories = Category::orderBy('display_order', 'asc')->get();
        return view('category.manage')->with('categories', $categories);
    }

    public function createForm(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required|min:1|string|max:255|unique:categories,name',
        ]);
        $category = new Category();
        $category->name = $request->name;
        $category->save();
        $category->display_order = $category->id;
        $category->save();
        $request->session()->flash('status', 'Category created.');
        return redirect()->route('category.manage');
    }

    public function editForm(Request $request, Category $category) {
        $validatedData = $request->validate([
            'edname' => 'required|min:1|string|max:255|unique:categories,name,'.$category->id,
        ]);
        $category->name = $request->edname;
        $category->save();
        $request->session()->flash('status', "Category {$category->name} edited.");
        return redirect()->route('category.manage');
    }

    public function deleteForm(Request $request, Category $category) {
        if ($category->id!=1) {
            $category->delete();
            $request->session()->flash('status', "Category {$category->name} deleted!");
        } else {
            $request->session()->flash('status', "Category {$category->name} can't be deleted!");
        }
        return redirect()->route('category.manage');
    }

    public function move(Category $category1, Category $category2) {
        $oldDisplayOrder = $category1->display_order;
        $newDisplayOrder = $category2->display_order;
        if ($newDisplayOrder<$oldDisplayOrder) {
            $categories = Category::whereBetween('display_order', [$newDisplayOrder, $oldDisplayOrder-1])->orderBy('display_order', 'asc')->get();
            for ($i=0; $i<sizeof($categories)-1; $i++) {
                $categories[$i]->display_order = $categories[$i+1]->display_order;
                $categories[$i]->save();
            }
            $categories[$i]->display_order = $oldDisplayOrder;
            $categories[$i]->save();
            $category1->display_order = $newDisplayOrder;
            $category1->save();
        } else if ($oldDisplayOrder<$newDisplayOrder) {
            $categories = Category::whereBetween('display_order', [$oldDisplayOrder+1, $newDisplayOrder])->orderBy('display_order', 'asc')->get();
            for ($i=sizeof($categories)-1; $i>=1; $i--) {
                $categories[$i]->display_order = $categories[$i-1]->display_order;
                $categories[$i]->save();
            }
            $categories[0]->display_order = $oldDisplayOrder;
            $categories[0]->save();
            $category1->display_order = $newDisplayOrder;
            $category1->save();
        }

        $displayOrder = $category1->display_order;
        $category1->display_order = $category2->display_order;
        $category2->display_order = $displayOrder;
        $category1->save();
        $category2->save();
        return redirect()->route('category.manage');
    }

    public function collapse(Category $category) {
        $category->collapse(\Auth::user());
        return Response::json();
    }

    public function expand(Category $category) {
        $category->expand(\Auth::user());
        return Response::json();
    }
}
