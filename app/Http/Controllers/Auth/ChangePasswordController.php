<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showChangeForm() {
        return view('auth.passwords.change');
    }

    public function change(Request $request) {
        $messages = [
            'old_password.required' => 'Please enter current password',
            'password.required' => 'Please enter new password',
        ];
        $validatedData = $request->validate([
            'old_password' => ['required',
                function ($attribute, $value, $fail) {
                    if (!Hash::check($value, \Auth::user()->password)) {
                        return $fail('Please enter correct current password');
                    }
                }
            ],
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password'
        ], $messages);
        \Auth::user()->password = bcrypt($request->input('password'));
        \Auth::user()->save();
        $request->session()->flash('status', 'Password changed!');
        return redirect()->route('home');
    }
}
