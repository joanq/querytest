<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\Group;
use App\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Teacher')->only('move');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $categories = Category::orderBy('display_order', 'asc')->get();
      $groups = Group::all();
      return view('home')->with(['categories'=>$categories, 'groups'=>$groups]);
    }

    public function move(Test $test1, Test $test2) {
        $test1->category_id = $test2->category_id;
        $oldDisplayOrder = $test1->display_order;
        $newDisplayOrder = $test2->display_order;
        if ($newDisplayOrder<$oldDisplayOrder) {
            $tests = Test::whereBetween('display_order', [$newDisplayOrder, $oldDisplayOrder-1])->orderBy('display_order', 'asc')->get();
            for ($i=0; $i<sizeof($tests)-1; $i++) {
                $tests[$i]->display_order = $tests[$i+1]->display_order;
                $tests[$i]->save();
            }
            $tests[$i]->display_order = $oldDisplayOrder;
            $tests[$i]->save();
            $test1->display_order = $newDisplayOrder;
            $test1->save();
        } else if ($oldDisplayOrder<$newDisplayOrder) {
            $tests = Test::whereBetween('display_order', [$oldDisplayOrder+1, $newDisplayOrder])->orderBy('display_order', 'asc')->get();
            for ($i=sizeof($tests)-1; $i>=1; $i--) {
                $tests[$i]->display_order = $tests[$i-1]->display_order;
                $tests[$i]->save();
            }
            $tests[0]->display_order = $oldDisplayOrder;
            $tests[0]->save();
            $test1->display_order = $newDisplayOrder;
            $test1->save();
        }

        $displayOrder = $test1->display_order;
        $test1->display_order = $test2->display_order;
        $test2->display_order = $displayOrder;
        $test1->save();
        $test2->save();
        return redirect('home');
    }
}
