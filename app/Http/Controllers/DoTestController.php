<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Test;
use App\Question;
use App\Message;
use App\Attempt;
use App\ResultsComparator;
use App\Events\QuestionSolved;
use App\Events\QuestionFailed;
use App\Events\TestSolved;

class DoTestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'test.published']);
        $this->middleware('test.open')->only('answerQuestion');
    }

    private function showResults(Test $test) {
        return view('test.userattempts')->with(['user'=>\Auth::user(), 'test'=>$test]);
    }

    public function showTest(Test $test) {
        if ($test->isClosed(\Auth::user()->group) && $test->areResultsShown(\Auth::user()->group)) {
            return $this->showResults($test);
        }
        $questions = $test->questions()->orderBy('id', 'asc')->withCount([
            'attempts'=>function($query) {
                $query->where('user_id', \Auth::user()->id);
            },
            'attempts as solved'=>function($query) {
                $query->where([
                    ['user_id', \Auth::user()->id],
                    ['message_id', Message::where('code', 'MATCH')->first()->id]
                ]);
            }
        ])->get();
        foreach ($questions as $question) {
            $question->attempts()->where('user_id', \Auth::user()->id);
        }
        return view('test.show')->with('test', $test)->with('questions', $questions);
    }

    private function saveAttempt(Test $test, Question $question, Message $message, $answer) {
        $user = \Auth::user();
        if ($test->areResultsHidden($user->group)) {
            // Save only last attempt
            $question->attempts()->where('user_id', $user->id)->delete();
            Attempt::create($user, $question, $message, $answer);
        } else {
            // Only count attempt if the question is not yet solved
            if (!$question->isSolved($user)) {
                Attempt::create($user, $question, $message, $answer);
                if ($message->code == 'MATCH') {
                    event(new QuestionSolved($user, $question));
                    // Have solved all test questions?
                    $nQuestions = $test->questions()->count();
                    $questionsSolved = $test->nQuestionsSolved($user);
                    if ($nQuestions == $questionsSolved) {
                        event(new TestSolved($user, $test));
                    }
                } else {
                    event(new QuestionFailed($user, $question));
                }
            }
        }
    }

    public function answerQuestion(Request $request, Question $question) {
        $answer = $request->input("answer$question->id");
        $test = $question->test;
        $resultsComparator = new ResultsComparator();
        if (!empty($answer)) {
            try {
                $message = $resultsComparator->checkAnswer($question, $answer);
                if ($test->isPublished(\Auth::user()->group)) {
                    $points = $this->saveAttempt($test, $question, $message, $answer);
                }
                if ($test->areResultsHidden(\Auth::user()->group)) {
                    $message->type='warning';
                    $message->message='Attempt saved.';
                }
            } catch (QueryException $e) {
                $request->session()->flash('status', "Error in question $question->id official answer: ".$e->getMessage());
                return redirect('home');
            }
        } else {
            $message = new Message();
            $message->type='error';
            $message->message='Empty answer.';
        }
        return redirect("test/$test->id/show#question$question->id")->with(['questionid'=>$question->id, 'message'=>$message])->withInput();
    }

    public function hint(Request $request, Question $question) {
        $message = Message::where('code', 'HINT')->firstOrFail();
        $n_attempts = $question->attempts()->where('user_id', \Auth::user()->id)->count();
        $n_hints = $question->attempts()->where('user_id', \Auth::user()->id)->where('message_id', $message->id)->count();
        $result = '';
        if ($n_attempts > 0) {
          if ($n_hints == 0) {
            $this->saveAttempt($question->test, $question, $message, '');
          }
          $result = $question->hint;
        }
        return $result;
    }
}
