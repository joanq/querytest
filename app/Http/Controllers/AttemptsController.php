<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Test;
use App\User;
use App\Attempt;
use App\Group;
use DateFormatting;
use App\ResultsComparator;

class AttemptsController extends Controller
{
    private static $pagination = 30;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Teacher')->except('showAttemptsForQuestion');
        $this->middleware('role.or.user:Teacher')->only('showAttemptsForQuestion');
    }

    public function delete(Request $request, Attempt $attempt) {
        $attempt->delete();
        return redirect()->back();
    }

    public function setOrderBy(Request $request) {
        $orderby = $request->input('orderby', 'date');
        if (!in_array($orderby, ['date','user','message','question']))
            $orderby='date';
        $request->session()->put('orderby', $orderby);
        $dir = $request->input('dir', 'asc');
        if (!in_array($dir, ['asc', 'desc']))
            $dir='asc';
        $request->session()->put('dir', $dir);
        return redirect()->back();
    }

    private function showAttempts(Request $request, Test $test, Question $question=null, User $user=null) {
      setlocale(LC_TIME, \App::getLocale());
      $orderby=$request->session()->get('orderby','date');
      $dir=$request->session()->get('dir','asc');
      $group=$request->session()->get('group','All');
      if (isset($question)) {
          $attempts = $question->attempts();
      } else {
          $attempts = $test->attempts();
      }
      $attempts = $attempts->join('users as u', 'u.id', '=', 'attempts.user_id');
      $attempts = $attempts->leftJoin('groups as g', 'g.id', '=', 'u.group_id');
      switch ($orderby) {
          case 'user':
              $attempts = $attempts->orderBy('u.lastname', $dir)->orderBy('u.firstname', $dir);
          break;
          case 'message':
              $attempts = $attempts->
                  join('messages as m', 'm.id', '=', 'attempts.message_id')->
                  orderBy('m.code', $dir);
          break;
          case 'question':
              if (!isset($question)) {
                  $attempts = $attempts->
                      join('questions as q', 'q.id', '=', 'attempts.question_id')->
                      orderBy('q.question', $dir);
                  break;
              }
          default: /* date */
              $orderby = 'date';
              $attempts = $attempts->orderBy('created_at', $dir);
      }
      if (isset($user)) {
          $attempts = $attempts->where('attempts.user_id', '=', $user->id);
      }
      if ($group!='All') {
          $attempts = $attempts->where('g.name', $group);
      }
      $attempts = $attempts->select('attempts.*')->
          with('user', 'message', 'question')->
          paginate(25);
      foreach ($attempts as $attempt) {
          $c = \Carbon\Carbon::instance($attempt->created_at);
          $attempt->date = $c->formatLocalized('%c');
      }
      $icon = $dir=='asc'?'&#8643;':'&#8638;';
      $dir=($dir=='asc'?'desc':'asc');
      $groups = Group::all();

      return view('test.attempts')->with(['question'=>isset($question)?$question:null,
          'test'=>$test, 'attempts'=>$attempts, 'orderby'=>$orderby, 'dir'=>$dir,
          'icon'=>$icon, 'groups'=>$groups, 'group'=>$group,
          'userid'=>isset($user)?$user->id:null]);
    }

    public function showAttemptsForTest(Request $request, Test $test, User $user=null) {
        return $this->showAttempts($request, $test, null, $user);
    }

    public function showAttemptsForQuestion(Request $request, Question $question, User $user=null) {
        return $this->showAttempts($request, $question->test, $question, $user);
    }

    public function showUserAttemptsForTest(Request $request, Test $test, User $user) {
        return view('test.userattempts')->with(['user'=>$user, 'test'=>$test]);
    }

    public function showSummaryForTest(Request $request, Test $test) {
        $group=$request->session()->get('group','All');
        $questions = \DB::table('questions')
            ->select('questions.id')
            ->where('questions.test_id', '=', $test->id)
            ->orderBy('id', 'asc')
            ->get();
        if ($group!='All') {
            $users = \DB::table('users')->select('users.id', 'users.firstname', 'users.lastname')
                ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
                ->where('groups.name', '=', $group)->orderBy('users.lastname')->orderBy('users.firstname')->paginate(AttemptsController::$pagination);
        } else {
            $users = \DB::table('users')->select('users.id', 'users.firstname', 'users.lastname')
                ->orderBy('users.lastname')->orderBy('users.firstname')->paginate(AttemptsController::$pagination);
        }
        $results = \DB::table('users')
            ->select('users.id as userid', 'attempts.question_id as questionid')
            ->selectRaw('max(case messages.type when "success" then 2 else 1 end) as message')
            ->selectRaw('count(attempts.id) as nTries')
            ->leftJoin('attempts', 'users.id', '=', 'attempts.user_id')
            ->leftJoin('messages', 'messages.id', '=', 'attempts.message_id')
            ->leftJoin('questions', 'questions.id', '=', 'attempts.question_id')
            ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
            ->where('questions.test_id', '=', $test->id)
            ->whereIn('users.id', $users->pluck('id'))
            ->groupBy('users.id', 'users.firstname', 'users.lastname', 'attempts.question_id');
        if ($group!='All') {
            $results = $results->where('groups.name', $group);
        }
        $results = $results->get();
        $groups = Group::all();

        return view('test.summary')->with(['test'=>$test,
            'group'=>$group, 'groups'=>$groups,
            'users'=>$users, 'results'=>$results, 'questions'=>$questions]);
    }

    public function showReevaluateForm(Request $request, Question $question) {
        $resultsComparator = new ResultsComparator();
        $modified = 0;
        $attempts = $question->attempts()->get();
        foreach ($attempts as $attempt) {
            if (!empty($attempt->answer)) {
                $message = $resultsComparator->checkAnswer($question, $attempt->answer);
                if ($message->id != $attempt->message_id) {
                    $modified++;
                }
            }
        }
        return view("question.reevaluate")->with(['test'=>$question->test, 'question'=>$question, 'n_attempts'=>$attempts->count(), 'modified'=>$modified]);
    }

    public function reevaluate(Request $request, Question $question) {
        $resultsComparator = new ResultsComparator();
        $attempts = $question->attempts()->get();
        foreach ($attempts as $attempt) {
            if (!empty($attempt->answer)) {
                $message = $resultsComparator->checkAnswer($question, $attempt->answer);
                $attempt->message_id = $message->id;
                $attempt->save();
            }
        }
        return redirect("question/$question->id/attempts");
    }
}
