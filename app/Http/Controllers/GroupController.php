<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:Admin']);
    }

    public function setGroupFilter(Request $request) {
        $request->session()->put('group', $request->input('group', 'All'));
        $url = url()->previous();
        if (strpos($url, '?')!==false) {
            $url = strstr($url, '?', true);
        }
        return redirect($url);
    }

    public function showManageForm(Request $request) {
        $groups = Group::orderBy('name', 'asc')->get();
        return view('group.manage')->with('groups', $groups);
    }

    public function createForm(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required|min:1|string|max:255|unique:groups,name',
            'description' => 'nullable'
        ]);
        $group = new Group();
        $group->name = $request->name;
        $group->description = $request->description===null?'':$request->description;
        $group->save();
        $request->session()->flash('status', 'Group created.');
        return redirect()->route('group.manage');
    }

    public function editForm(Request $request, Group $group) {
        $validatedData = $request->validate([
            'edname' => 'required|min:1|string|max:255|unique:groups,name,'.$group->id,
            'eddescription' => 'nullable'
        ]);
        $group->name = $request->edname;
        $group->description = $request->eddescription===null?'':$request->eddescription;
        $group->save();
        $request->session()->flash('status', "Group {$group->name} edited.");
        return redirect()->route('group.manage');
    }

    public function deleteForm(Request $request, Group $group) {
        $group->delete();
        $request->session()->flash('status', "Group {$group->id} deleted!");
        return redirect()->route('group.manage');
    }
}
