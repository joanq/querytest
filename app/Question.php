<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function test() {
        return $this->belongsTo('App\Test');
    }

    public function attempts() {
        return $this->hasMany('App\Attempt');
    }

    public function isSolved($user) {
        $matchId = Message::where('code', 'MATCH')->first()->id;
        $solved = $this->attempts()->where([
            ['user_id', $user->id],
            ['message_id', $matchId]
        ])->count();
        return $solved>0;
    }

    public function clone() {
        $newquestion = new Question();
        $newquestion->solution = $this->solution;
        $newquestion->question = $this->question;
        $newquestion->hint = $this->hint;
        return $newquestion;
    }
}
