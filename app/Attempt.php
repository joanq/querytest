<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attempt extends Model
{
    public function message() {
        return $this->belongsTo('App\Message');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function question() {
        return $this->belongsTo('App\Question');
    }

    public static function create(User $user, Question $question, Message $message, $answer) {
        $attempt = new Attempt();
        $attempt->user_id = $user->id;
        $attempt->question_id = $question->id;
        $attempt->message_id = $message->id;
        $attempt->answer = $answer;
        $attempt->save();
        return $attempt;
    }
}
