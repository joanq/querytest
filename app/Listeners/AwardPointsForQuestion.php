<?php

namespace App\Listeners;

use App\Events\QuestionSolved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Message;

class AwardPointsForQuestion
{
    // Points for getting a correct answer
    private static $basePoints = 10;
    // Points lost for each additional attempt
    private static $attemptLoss = 2;
    // Minimum points for getting a correct answer
    private static $minPoints = 4;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuestionSolved  $event
     * @return void
     */
    public function handle(QuestionSolved $event)
    {
        $user = $event->user;
        $question = $event->question;
        $points = AwardPointsForQuestion::$basePoints;
        // Number of attempts
        $nAttempts = $question->attempts()->where('user_id', $user->id)->count();
        $points -= AwardPointsForQuestion::$attemptLoss * ($nAttempts-1);
        // Minimum
        if ($points < AwardPointsForQuestion::$minPoints) {
            $points = AwardPointsForQuestion::$minPoints;
        }
        $user->points += $points;
        $user->save();
        session()->flash('questionPoints', $points);
    }
}
