<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Events\TestCreated;
use App\Group;
use App\Test;
use App\TestState;

class CreateTestStatesOnNewTest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TestCreated $event)
    {
        $test = $event->test;
        $groups = Group::all();
        foreach ($groups as $group) {
            $testState = new TestState();
            $testState->test_id = $test->id;
            $testState->group_id = $group->id;
            $testState->save();
        }
    }
}
