<?php

namespace App\Listeners;

use App\Events\QuestionFailed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Achievements\UserFailed50Attempts;
use App\Achievements\UserFailed100Attempts;
use App\Achievements\UserFailed200Attempts;
use App\Achievements\UserFailed500Attempts;
use App\Achievements\UserFailed1000Attempts;

use App\Achievements\UserMade10ConsecutiveRightAnswers;
use App\Achievements\UserMade20ConsecutiveRightAnswers;
use App\Achievements\UserMade40ConsecutiveRightAnswers;
use App\Achievements\UserMade60ConsecutiveRightAnswers;
use App\Achievements\UserMade100ConsecutiveRightAnswers;

class AchievementsForFailingQuestion
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuestionFailed  $event
     * @return void
     */
    public function handle(QuestionFailed $event)
    {
        $event->user->addProgress(new UserFailed50Attempts(), 1);
        $event->user->addProgress(new UserFailed100Attempts(), 1);
        $event->user->addProgress(new UserFailed200Attempts(), 1);
        $event->user->addProgress(new UserFailed500Attempts(), 1);
        $event->user->addProgress(new UserFailed1000Attempts(), 1);

        $event->user->setProgress(new UserMade10ConsecutiveRightAnswers(), 0);
        $event->user->setProgress(new UserMade20ConsecutiveRightAnswers(), 0);
        $event->user->setProgress(new UserMade40ConsecutiveRightAnswers(), 0);
        $event->user->setProgress(new UserMade60ConsecutiveRightAnswers(), 0);
        $event->user->setProgress(new UserMade100ConsecutiveRightAnswers(), 0);
    }
}
