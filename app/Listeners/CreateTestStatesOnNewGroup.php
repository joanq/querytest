<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Events\GroupCreated;
use App\Group;
use App\Test;
use App\TestState;

class CreateTestStatesOnNewGroup
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(GroupCreated $event)
    {
        $group = $event->group;
        $tests = Test::all();
        foreach ($tests as $test) {
            $testState = new TestState();
            $testState->group_id = $group->id;
            $testState->test_id = $test->id;
            $testState->save();
        }
    }
}
