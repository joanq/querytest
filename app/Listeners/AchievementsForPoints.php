<?php

namespace App\Listeners;

use App\Events\PointsChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Achievements\UserReached50Points;
use App\Achievements\UserReached100Points;
use App\Achievements\UserReached200Points;
use App\Achievements\UserReached500Points;
use App\Achievements\UserReached1000Points;
use App\Achievements\UserReached1500Points;
use App\Achievements\UserReached2000Points;

class AchievementsForPoints
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PointsChanged  $event
     * @return void
     */
    public function handle(PointsChanged $event)
    {
        $event->user->setProgress(new UserReached50Points(), $event->user->points);
        $event->user->setProgress(new UserReached100Points(), $event->user->points);
        $event->user->setProgress(new UserReached200Points(), $event->user->points);
        $event->user->setProgress(new UserReached500Points(), $event->user->points);
        $event->user->setProgress(new UserReached1000Points(), $event->user->points);
        $event->user->setProgress(new UserReached1500Points(), $event->user->points);
        $event->user->setProgress(new UserReached2000Points(), $event->user->points);
    }
}
