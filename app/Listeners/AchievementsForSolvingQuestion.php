<?php

namespace App\Listeners;

use App\Events\QuestionSolved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Achievements\UserSolved1Question;
use App\Achievements\UserSolved25Questions;
use App\Achievements\UserSolved50Questions;
use App\Achievements\UserSolved100Questions;
use App\Achievements\UserSolved200Questions;
use App\Achievements\UserSolved500Questions;
use App\Achievements\UserSolved20QuestionsAtFirstAttempt;
use App\Achievements\UserSolved50QuestionsAtFirstAttempt;
use App\Achievements\UserSolved100QuestionsAtFirstAttempt;
use App\Achievements\UserSolved200QuestionsAtFirstAttempt;
use App\Achievements\UserFailed10TimesAndThenSolvedQuestion;
use App\Achievements\UserMade10ConsecutiveRightAnswers;
use App\Achievements\UserMade20ConsecutiveRightAnswers;
use App\Achievements\UserMade40ConsecutiveRightAnswers;
use App\Achievements\UserMade60ConsecutiveRightAnswers;
use App\Achievements\UserMade100ConsecutiveRightAnswers;

class AchievementsForSolvingQuestion
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuestionSolved  $event
     * @return void
     */
    public function handle(QuestionSolved $event)
    {
        $event->user->addProgress(new UserSolved1Question(), 1);
        $event->user->addProgress(new UserSolved25Questions(), 1);
        $event->user->addProgress(new UserSolved50Questions(), 1);
        $event->user->addProgress(new UserSolved100Questions(), 1);
        $event->user->addProgress(new UserSolved200Questions(), 1);
        $event->user->addProgress(new UserSolved500Questions(), 1);

        UserSolved20QuestionsAtFirstAttempt::questionSolved($event->user, $event->question);
        UserSolved50QuestionsAtFirstAttempt::questionSolved($event->user, $event->question);
        UserSolved100QuestionsAtFirstAttempt::questionSolved($event->user, $event->question);
        UserSolved200QuestionsAtFirstAttempt::questionSolved($event->user, $event->question);

        UserFailed10TimesAndThenSolvedQuestion::questionSolved($event->user, $event->question);

        $event->user->addProgress(new UserMade10ConsecutiveRightAnswers(), 1);
        $event->user->addProgress(new UserMade20ConsecutiveRightAnswers(), 1);
        $event->user->addProgress(new UserMade40ConsecutiveRightAnswers(), 1);
        $event->user->addProgress(new UserMade60ConsecutiveRightAnswers(), 1);
        $event->user->addProgress(new UserMade100ConsecutiveRightAnswers(), 1);
    }
}
