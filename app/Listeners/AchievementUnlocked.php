<?php

namespace App\Listeners;

use Assada\Achievements\Event\Unlocked;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AchievementUnlocked
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Unlocked  $event
     * @return void
     */
    public function handle(Unlocked $event)
    {
        $achievement = $event->progress->details->getClass();
        if (session()->exists('achievements')) {
            session()->push('achievements', $achievement);
        } else {
            session()->flash('achievements', array($achievement));
        }
    }
}
