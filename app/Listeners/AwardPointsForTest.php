<?php

namespace App\Listeners;

use App\Events\TestSolved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AwardPointsForTest
{
    // Extra points for completing a whole test (per question)
    private static $testPoints = 2;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TestSolved  $event
     * @return void
     */
    public function handle(TestSolved $event)
    {
        $user = $event->user;
        $test = $event->test;
        $nQuestions = $test->questions()->count();
        $points = AwardPointsForTest::$testPoints * $nQuestions;
        $user->points += $points;
        $user->save();
        session()->flash('testPoints', $points);
    }
}
