<?php

namespace App\Listeners;

use App\Events\TestSolved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Achievements\UserSolved1Test;
use App\Achievements\UserSolved3Tests;
use App\Achievements\UserSolved5Tests;
use App\Achievements\UserSolved10Tests;
use App\Achievements\UserSolved15Tests;
use App\Achievements\UserSolved20Tests;

class AchievementsForSolvingTest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TestSolved  $event
     * @return void
     */
    public function handle(TestSolved $event)
    {
        $event->user->addProgress(new UserSolved1Test(), 1);
        $event->user->addProgress(new UserSolved3Tests(), 1);
        $event->user->addProgress(new UserSolved5Tests(), 1);
        $event->user->addProgress(new UserSolved10Tests(), 1);
        $event->user->addProgress(new UserSolved15Tests(), 1);
        $event->user->addProgress(new UserSolved20Tests(), 1);
    }
}
