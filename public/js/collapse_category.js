$('.collapse').on('hide.bs.collapse', function () {
  var category_id = this.id.split("-")[1];
  $("#btn-collapse-"+category_id).text("\u{25B6}");
  send(category_id, 'collapse');
})

$('.collapse').on('show.bs.collapse', function () {
  var category_id = this.id.split("-")[1];
  $("#btn-collapse-"+category_id).text("\u{25BC}");
  send(category_id, 'expand');
})

function send(category_id, verb) {
  $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
       }
  });
  var formData = {
      category: category_id,
  };
  var ajaxurl = 'category/'+category_id+'/'+verb;
  $.ajax({
     type: "POST",
     url: ajaxurl,
     data: formData,
     dataType: 'json',
     success: function (data) {},
     error: function (data) {
         console.log(data);
     }
  });
}
