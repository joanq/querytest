$(document).ready(function() {
  $('#add-question').click(function() {
    var $group=$('#questions .question-group:hidden:first');
    $group.show();
    $group.find('textarea').removeAttr('disabled');
    $(this).blur();
  });
  $('#remove-question').click(function() {
    var $group=$('#questions .question-group:visible:last')
    $group.hide();
    $group.find('textarea').attr('disabled', 'disabled');
    $(this).blur();
  });
});
