function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id.slice(5));
}
function drop(ev, itemType) {
    ev.preventDefault();
    var item1 = ev.dataTransfer.getData("text");
    var item2 = $(ev.target).closest(".row").attr('id').slice(5);
    window.location.href = "/"+itemType+"/"+item1+"/moveto/"+item2;
}
