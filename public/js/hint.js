$(document).ready(function() {
  $('.hint-btn').click(function(e) {
    e.preventDefault();
    var questionId = $(this).attr('id').split('-')[2];
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "/test/hint/"+questionId,
      method: 'post',
      success: function(result){
        $('#hint-text-'+questionId+' > div > span').text(result);
        $('#hint-btn-'+questionId).prop("disabled", true);
        $('#hint-text-'+questionId).show();
      }
    });
  });
});
