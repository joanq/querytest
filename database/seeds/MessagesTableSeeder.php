<?php

/*
This file is part of QueryTest.

QueryTest is free software: you can redistribute it and/or modify
it under the terms of the GNU Afferi General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QueryTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Afferi General Public License
along with QueryTest.  If not, see <https://www.gnu.org/licenses/>.
*/

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->insert([
            [
                'code'=>'MATCH',
                'message'=>'Results match!',
                'type'=>'success'
            ],
            [
                'code'=>'SQL_ERROR',
                'message'=>'SQL error!',
                'type'=>'error'
            ],
            [
                'code'=>'ROWS_NUMBER',
                'message'=>'Number of rows differ.',
                'type'=>'warning'
            ],
            [
                'code'=>'COLS_NUMBER',
                'message'=>'Number of columns differ.',
                'type'=>'warning'
            ],
            [
                'code'=>'NO_MATCH',
                'message'=>'Results doesn\'t match.',
                'type'=>'warning'
            ],
            [
                'code'=>'ORDER',
                'message'=>'Bad ordering.',
                'type'=>'warning'
            ],
            [
                'code'=>'HINT',
                'message'=>'Requested a hint.',
                'type'=>'warning'
            ]
        ]);
    }
}
