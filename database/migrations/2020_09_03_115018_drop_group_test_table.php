<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropGroupTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('group_test');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('group_test', function (Blueprint $table) {
            $table->biginteger('group_id')->unsigned();
            $table->biginteger('test_id')->unsigned();
            $table->primary(['group_id', 'test_id']);
            $table->boolean('published')->default(false);
            $table->boolean('closed')->default(false);
            $table->boolean('hide_results')->default(false);
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->foreign('test_id')->references('id')->on('tests')->onDelete('cascade');
        });
    }
}
