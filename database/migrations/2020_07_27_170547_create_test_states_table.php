<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_states', function (Blueprint $table) {
            $table->id();
            $table->biginteger('group_id')->unsigned();
            $table->biginteger('test_id')->unsigned();
            $table->unique(['group_id', 'test_id']);
            $table->boolean('published')->default(false);
            $table->boolean('hide_results')->default(false);
            $table->datetime('open_from')->nullable()->default(null);
            $table->datetime('open_until')->nullable()->default(null);
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->foreign('test_id')->references('id')->on('tests')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_states');
    }
}
