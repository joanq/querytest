<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('password/change', 'Auth\ChangePasswordController@showChangeForm')->name('password.change');
Route::post('password/change', 'Auth\ChangePasswordController@change');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('test/{test1}/moveto/{test2}', 'HomeController@move')->name('test.move');

// Admin Routes...
Route::get('test/create', 'TestController@showCreateForm')->name('test.create');
Route::get('test/{test}/edit', 'TestController@showEditForm')->name('test.edit');
Route::post('test/create', 'TestController@createForm');
Route::post('test/{test}/edit', 'TestController@editForm');
Route::get('test/{test}/delete', 'TestController@showDeleteForm')->name('test.delete');
Route::post('test/{test}/delete', 'TestController@deleteForm');
Route::get('test/{test}/visibility', 'TestController@showVisibilityForm')->name('test.visibility');
Route::post('test/{test}/visibility/{group}', 'TestController@visibilityForm')->name('test.setvisibility');
Route::post('test/{test}/visibility', 'TestController@visibilityForm')->name('test.changevisibility');
Route::get('test/{test}/clone', 'TestController@clone')->name('test.clone');

Route::get('user/{user}/profile', 'UserController@showProfile')->name('user.profile');
Route::get('user/list', 'UserController@showList')->name('user.list');
Route::get('user/{user}/edit', 'UserController@showEditForm')->name('user.edit');
Route::post('user/{user}/edit', 'UserController@editForm');
Route::get('user/{user}/delete', 'UserController@showDeleteForm')->name('user.delete');
Route::post('user/{user}/delete', 'UserController@deleteUser');
Route::get('user/create', 'UserController@showCreateForm')->name('user.create');
Route::post('user/create', 'UserController@createForm');
Route::get('group/manage', 'GroupController@showManageForm')->name('group.manage');
Route::post('group/create', 'GroupController@createForm')->name('group.create');
Route::post('group/{group}/edit', 'GroupController@editForm')->name('group.edit');
Route::get('group/{group}/delete', 'GroupController@deleteForm')->name('group.delete');
Route::get('user/csv', 'UserController@showFromCSVForm')->name('user.fromcsv');
Route::post('user/csv', 'UserController@createFromCSV');

Route::get('ranking', 'RankingController@show')->name('ranking');

// Test Routes...
Route::get('test/{test}/show', 'DoTestController@showTest')->name('test.show');
Route::post('test/answer/{question}', 'DoTestController@answerQuestion')->name('question.answer');
Route::post('test/hint/{question}', 'DoTestController@hint')->name('question.hint');

Route::get('test/{test}/attempts', 'AttemptsController@showAttemptsForTest')->name('test.attempts');
Route::get('test/{test}/user/{user}/attempts/', 'AttemptsController@showUserAttemptsForTest')->name('testuser.attempts');
Route::get('test/{test}/summary', 'AttemptsController@showSummaryForTest')->name('test.summary');
Route::get('question/{question}/attempts', 'AttemptsController@showAttemptsForQuestion')->name('question.attempts');
Route::get('question/{question}/user/{user}/attempts', 'AttemptsController@showAttemptsForQuestion')->name('user.attempts');
Route::get('question/{question}/reevaluate', 'AttemptsController@showReevaluateForm')->name('question.reevaluate');
Route::post('question/{question}/reevaluate', 'AttemptsController@reevaluate');
Route::get('attempt/{attempt}/delete', 'AttemptsController@delete')->name('attempt.delete');

Route::post('filter/group', 'GroupController@setGroupFilter')->name('filter.group');
Route::get('filter/orderby', 'AttemptsController@setOrderBy')->name('filter.orderby');
Route::get('filter/usersorderby', 'UserController@setOrderBy')->name('filter.usersorderby');

Route::get('category/manage', 'CategoryController@showManageForm')->name('category.manage');
Route::post('category/create', 'CategoryController@createForm')->name('category.create');
Route::post('category/{category}/edit', 'CategoryController@editForm')->name('category.edit');
Route::get('category/{category}/delete', 'CategoryController@deleteForm')->name('category.delete');
Route::get('category/{category1}/moveto/{category2}', 'CategoryController@move')->name('category.move');

Route::post('category/{category}/collapse', 'CategoryController@collapse')->name('category.collapse');
Route::post('category/{category}/expand', 'CategoryController@expand')->name('category.expand');
